/**
 * Copyright (c) 2012 - 2022 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.emf.ttl.configuration;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.gecko.emftriple.jena.resource.TTLResourceFactory;

/**
 * 
 * @author ilenia
 * @since Aug 18, 2022
 */
public class ConfigurableTTLResourceFactory extends TTLResourceFactory {

	@Override
	public Resource createResource(URI uri) {
		return new ConfigurableTTLResource(uri);
	}
}
