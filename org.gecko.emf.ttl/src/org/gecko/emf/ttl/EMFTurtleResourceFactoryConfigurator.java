/**
 * Copyright (c) 2012 - 2018 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.emf.ttl;

import org.eclipse.emf.ecore.resource.Resource.Factory.Registry;
import org.gecko.emf.osgi.ResourceFactoryConfigurator;
import org.gecko.emf.osgi.annotation.EMFResourceFactoryConfigurator;
import org.gecko.emf.osgi.annotation.provide.ProvideEMFResourceConfigurator;
import org.gecko.emf.ttl.configuration.ConfigurableTTLResourceFactory;
import org.gecko.emf.ttl.constants.EMFTurtle;
import org.gecko.emftriple.jena.resource.TTLResourceFactory;
import org.osgi.service.component.annotations.Component;

/**
 * Implementation of the {@link IResourceSetConfigurator} to provide support for {@link TurtleResourceImpl}.
 * 
 * It provides the {@link TurtleResourceFactoryImpl} for the following identifiers:
 * <ul>
 * 	<li>Extension: ttl
 * 	<li>contentType: application/ttl
 * 	<li>contentType: application/ttl
 * 	<li>Protocol: ttl
 * </ul>
 * 
 * @author ilenia
 * @since Aug 18, 2022
 */
@Component(name="EMFTurtleConfigurator", immediate=true, service=ResourceFactoryConfigurator.class)
@ProvideEMFResourceConfigurator(
	name = EMFTurtle.EMFTTL_CAPABILITY_NAME, 
	contentType = {
			"application/ttl"
	},
	fileExtension = {
			"ttl"
	},
	version = "1.0.1"
)
@EMFResourceFactoryConfigurator(
		name = EMFTurtle.EMFTTL_CAPABILITY_NAME, 
		fileExtension = {
				"ttl"
		},
		contentType = {
				"application/ttl"
			}
		)
public class EMFTurtleResourceFactoryConfigurator implements ResourceFactoryConfigurator{

	/* 
	 * (non-Javadoc)
	 * @see org.gecko.emf.osgi.ResourceFactoryConfigurator#configureResourceFactory(org.eclipse.emf.ecore.resource.Resource.Factory.Registry)
	 */
	@Override
	public void configureResourceFactory(Registry registry) {
		registry.getExtensionToFactoryMap().put("ttl", createConfigurableFactory());
		registry.getContentTypeToFactoryMap().put("application/ttl", createConfigurableFactory());		
	}

	/* 
	 * (non-Javadoc)
	 * @see org.gecko.emf.osgi.ResourceFactoryConfigurator#unconfigureResourceFactory(org.eclipse.emf.ecore.resource.Resource.Factory.Registry)
	 */
	@Override
	public void unconfigureResourceFactory(Registry registry) {
		registry.getExtensionToFactoryMap().remove("ttl");
		registry.getContentTypeToFactoryMap().remove("application/ttl");		
	}

	private TTLResourceFactory createConfigurableFactory() {
		return new ConfigurableTTLResourceFactory();
	}
	

}
