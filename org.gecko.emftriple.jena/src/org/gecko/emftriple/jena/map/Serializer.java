/**
 * Copyright (c) 2012 - 2022 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.emftriple.jena.map;

import java.util.Collection;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.gecko.emftriple.apis.RDF;
import org.gecko.emftriple.jena.helper.EcoreTTLHelper;

/**
 * 
 * @author ilenia
 * @since Aug 23, 2022
 */
public class Serializer {

	public Model to(org.eclipse.emf.ecore.resource.Resource resource, Model graph) {
		resource.getContents().stream().forEach(c -> {
			to(c, graph);
		});
		return graph;
	}

	public Model to(EObject eObject, Model graph) {
		Resource subject = EcoreTTLHelper.getResource(eObject, graph);
		createTypeStatement(eObject, graph);
		eObject.eClass().getEAllAttributes().forEach(att -> {
			serialize(att, eObject, subject, graph);
		});
		
		eObject.eClass().getEAllReferences().forEach(ref -> {
			serialize(ref, eObject, subject, graph);
		});
		return graph;
	}
	
	private void createTypeStatement(EObject eObject, Model graph) {
		Property predicate = graph.getProperty(RDF.type);
		Resource object = EcoreTTLHelper.getResource(eObject.eClass(), graph);
		graph.add(EcoreTTLHelper.getResource(eObject, graph), predicate, object);		
	}

	@SuppressWarnings("unchecked")
	private void serialize(EAttribute attribute, EObject eObject, Resource resource, Model graph) {
		if (attribute.isDerived() || attribute.isTransient() || !eObject.eIsSet(attribute)) return;

		Object value = eObject.eGet(attribute);
		if (attribute.isMany()) {
			((Collection<Object>) value).forEach(v -> {
				serializeOne(v, attribute, resource, graph);
			});
		}
		else
			serializeOne(value, attribute, resource, graph);
	}

	private void serializeOne(Object value, EAttribute attribute, Resource resource, Model graph) {
		graph.add(resource, EcoreTTLHelper.getProperty(attribute, graph), EcoreTTLHelper.getLiteral(value, attribute, graph));
	}

	@SuppressWarnings("unchecked")
	private void serialize(EReference reference, EObject eObject, Resource resource, Model graph) {
		if (reference.isDerived() || reference.isTransient() || !eObject.eIsSet(reference)) return;
		
		Object value = eObject.eGet(reference);
		if (reference.isMany()) {
			((Collection<Object>) value).forEach(v -> {
				serializeOne((EObject) v, reference, resource, graph);
			});
		}
		else
			serializeOne((EObject) value, reference, resource, graph);
	}

	private void serializeOne(EObject value, EReference reference, Resource resource, Model graph) {
		if (reference.isContainment()) to(value, graph);
		graph.add(resource, EcoreTTLHelper.getProperty(reference, graph), EcoreTTLHelper.getResource(value, graph));
	}

}
