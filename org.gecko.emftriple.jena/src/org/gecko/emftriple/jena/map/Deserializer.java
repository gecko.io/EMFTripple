/**
 * Copyright (c) 2012 - 2022 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.emftriple.jena.map;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ResIterator;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.rdf.model.StmtIterator;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.gecko.emftriple.apis.RDF;
import org.gecko.emftriple.jena.helper.EcoreTTLHelper;

/**
 * 
 * @author ilenia
 * @since Aug 23, 2022
 */
public class Deserializer {

	public void from(Model graph, org.eclipse.emf.ecore.resource.Resource resource) {
		ResourceSet resourceSet = resource.getResourceSet();
		List<EObject> contents = resource.getContents();
		Map<Resource, EObject> mapOfObjects = new HashMap<>();
		List<EObject> listOfObjects = new ArrayList<>();

		ResIterator resIter = graph.listSubjects();
		while(resIter.hasNext()) {
			appendTo(from(resIter.next(), mapOfObjects, resourceSet), listOfObjects);
		}
//		original was
//		graph.listSubjects.fold(listOfObjects, [list, it |
//			from(it,  mapOfObjects, resourceSet).appendTo(list)
//		])

		mapOfObjects.entrySet().forEach(e -> {
			Resource res = e.getKey();
			EObject eObject = e.getValue();
			EClass eClass = eObject.eClass();
			eClass.getEAllReferences().stream().forEach(ref -> {
				deSerialize(ref, res, eObject, mapOfObjects, resourceSet);
			});
		});
		
		mapOfObjects.values().forEach(v -> {
			if(v.eContainer() == null) {
				contents.add(v);
			}
		});
	}

	public EObject from(Resource res, Map<Resource, EObject> mapOfObjects, ResourceSet resourceSet) {
		Statement statement = res.getProperty(res.getModel().getProperty(RDF.type));
		EObject eObject = createEObject(statement, res, mapOfObjects, resourceSet);
		return eObject;	
//		original was:
//		switch (statement) {
//			Statement case statement.object.URIResource: createEObject(statement, res, mapOfObjects, resourceSet)
//			default: null
//		}
	}

	protected void appendTo(EObject object, List<EObject> objects) {
		if (object != null) objects.add(object);
	}

	protected EObject createEObject(Statement stmt, Resource res, Map<Resource, EObject> mapOfObjects, ResourceSet resourceSet) {
		EObject eObject = EcoreTTLHelper.getEObject(resourceSet, stmt.getObject().asResource());
		if(eObject instanceof EClass) {
			EClass eClass = (EClass) eObject;
			EObject eObj = EcoreTTLHelper.create(eClass); 
			eClass.getEAllAttributes().forEach(att -> {
				deSerialize(att, res, eObj);
			});
			mapOfObjects.put(res, eObj);
			return eObj;
		}		
		return null;
	}

	@SuppressWarnings("unchecked")
	public void deSerialize(EAttribute attribute, Resource resource, EObject eObject) {
		if (attribute.isDerived() || attribute.isTransient()) return;

		StmtIterator stmts = resource.listProperties(EcoreTTLHelper.getProperty(attribute, resource.getModel()));

		if (attribute.isMany()) {
			Collection<Object> values = (Collection<Object>) eObject.eGet(attribute);
			while(stmts.hasNext()) {
				Object v = EcoreUtil.createFromString(attribute.getEAttributeType(), stmts.nextStatement().getObject().asLiteral().getLexicalForm());
				if (v != null) values.add(v);
			}
		} else if (stmts.hasNext()) {
			Object v = EcoreUtil.createFromString(attribute.getEAttributeType(), stmts.next().getObject().asLiteral().getLexicalForm());
//			original code was stmts.head.object.asLiteral.lexicalForm
			if (v != null) eObject.eSet(attribute, v);
		}
	}

	@SuppressWarnings("unchecked")
	public void deSerialize(EReference reference, Resource resource, EObject eObject, Map<Resource, EObject> mapOfObjects, ResourceSet resourceSet) {
		if (reference.isDerived() || reference.isTransient()) return;

		StmtIterator stmts = resource.listProperties(EcoreTTLHelper.getProperty(reference, resource.getModel()));
		
		if (reference.isMany()) {
			Collection<Object> values = (Collection<Object>) eObject.eGet(reference);
			while(stmts.hasNext()) {
				Resource v = stmts.nextStatement().getObject().asResource();
				EObject o = mapOfObjects.get(v);
				if (o != null) values.add(o);
			}
		} else if (stmts.hasNext()) {
//			original was it.head.object.asResource
			Resource v = stmts.nextStatement().getObject().asResource();
			EObject o = mapOfObjects.get(v);
			if (o != null) eObject.eSet(reference, o);
		}
	}
}
