/**
 * Copyright (c) 2012 - 2022 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.emftriple.jena.map;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.eclipse.emf.ecore.resource.Resource;

/**
 * 
 * @author ilenia
 * @since Aug 23, 2022
 */
public class EObjectMapper {
	
	public Model to(Resource resource) {
		return to(ModelFactory.createDefaultModel(), resource);
	}

	public Model to(Model model, Resource resource) {
		Serializer serializer = new Serializer();
		serializer.to(resource, (model == null) ? ModelFactory.createDefaultModel() : model);
		return model;
	}

	public Model from(Model graph, Resource resource) {
		Deserializer deserializer = new Deserializer();
		deserializer.from(graph, resource);
		return graph;
	}


}
