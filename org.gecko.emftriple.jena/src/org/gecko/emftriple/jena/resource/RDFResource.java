/**
 * Copyright (c) 2012 - 2022 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.emftriple.jena.resource;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Map;

import org.apache.jena.rdf.model.Model;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.URIConverter.Loadable;
import org.eclipse.emf.ecore.resource.URIConverter.Saveable;
import org.eclipse.emf.ecore.resource.impl.ResourceImpl;
import org.gecko.emftriple.jena.io.RDFReader;
import org.gecko.emftriple.jena.io.RDFWriter;
import org.gecko.emftriple.jena.map.EObjectMapper;

/**
 * 
 * @author ilenia
 * @since Aug 23, 2022
 */
public class RDFResource extends ResourceImpl{
	
	public RDFResource() {
	}

	public RDFResource(URI uri) {
		super(uri);
	}

	@Override 
	protected void doLoad(InputStream inputStream, Map<?, ?> options) throws IOException {
		if (inputStream instanceof Loadable) {
			((Loadable) inputStream).loadResource(this);
		} else {
			EObjectMapper mapper = new EObjectMapper();
			mapper.from(read(inputStream), this);
		}
	}

	@Override 
	protected void doSave(OutputStream outputStream, Map<?, ?> options) throws IOException {
		if (outputStream instanceof Saveable) {
			((Saveable) outputStream).saveResource(this);
		} else {
			EObjectMapper mapper = new EObjectMapper();
			write(outputStream, mapper.to(this));
		}
	}
	
	protected Model read(InputStream stream) {
		return RDFReader.read(stream, null);
	}

	protected void write(OutputStream stream, Model graph) {
		RDFWriter.write(stream, graph, null);
	}


}
