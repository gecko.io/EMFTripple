/**
 * Copyright (c) 2012 - 2022 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.emftriple.jena.resource;

import java.io.InputStream;
import java.io.OutputStream;

import org.apache.jena.rdf.model.Model;
import org.eclipse.emf.common.util.URI;
import org.gecko.emftriple.apis.RDFFormat;
import org.gecko.emftriple.jena.io.RDFReader;
import org.gecko.emftriple.jena.io.RDFWriter;

/**
 * 
 * @author ilenia
 * @since Aug 23, 2022
 */
public class NTResource extends RDFResource {
	
	public NTResource() {
		super();
	}
	
	public NTResource(URI uri) {
		super(uri);
	}

	@Override 
	protected void write(OutputStream stream, Model graph) {
		RDFWriter.write(stream, graph, RDFFormat.NTRIPLES);
	}

	@Override 
	protected Model read(InputStream stream) {
		return RDFReader.read(stream, RDFFormat.NTRIPLES);
	}


}
