/**
 * Copyright (c) 2012 - 2022 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.emftriple.jena.io;

import java.io.InputStream;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.riot.RIOT;
import org.gecko.emftriple.apis.RDFFormat;

/**
 * 
 * @author ilenia
 * @since Aug 23, 2022
 */
public class RDFReader {

	public static Model read(InputStream stream, RDFFormat format) {
		return read(stream, ModelFactory.createDefaultModel(), format);
	}

	public static Model read(InputStream stream, Model graph, RDFFormat format) {
		switch (format) {
		case TURTLE: 
			return readTurtle(stream, graph);
		case NTRIPLES: 
			return readNTriples(stream, graph);
		case RDFJSON: 
			return readJson(stream, graph);
		default: 
			return readXML(stream, graph);
		}
	}

	private static Model readNTriples(InputStream stream, Model graph) {
		graph.getReader("N-TRIPLES").read(graph, stream, null);
		return graph;
	}

	private static Model readTurtle(InputStream stream, Model graph) {
		graph.getReader("TURTLE").read(graph, stream, null);
		return graph;
	}

	private static Model readJson(InputStream stream, Model graph) {
		RIOT.init();
		graph.getReader("RDF/JSON").read(graph, stream, null);
		return graph;
	}

	private static Model readXML(InputStream stream, Model graph) {
		graph.read(stream, null);
		return graph;
	}



}
