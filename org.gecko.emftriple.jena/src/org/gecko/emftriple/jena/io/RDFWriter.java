/**
 * Copyright (c) 2012 - 2022 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.emftriple.jena.io;

import java.io.OutputStream;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.riot.RIOT;
import org.gecko.emftriple.apis.RDFFormat;

/**
 * 
 * @author ilenia
 * @since Aug 23, 2022
 */
public class RDFWriter {
	
	public static void write(OutputStream stream, Model graph, RDFFormat format) {
		switch (format) {
			case TURTLE:
				writeTurtle(stream, graph);
				break;
			case NTRIPLES: 
				writeNTriples(stream, graph);
				break;
			case RDFJSON:
				writeJson(stream, graph);
				break;
			default:
				writeXML(stream, graph);
		}
	}

	private static void writeNTriples(OutputStream stream, Model model) {
		model.write(stream, "N-TRIPLES");
	}

	private static void writeTurtle(OutputStream stream, Model model) {
		model.write(stream, "TTL");
	}
	
	private static void writeJson(OutputStream stream, Model model) {
		RIOT.init();
		model.write(stream, "RDF/JSON");
	}

	private static void writeXML(OutputStream stream, Model model) {
		model.write(stream);
	}

}
