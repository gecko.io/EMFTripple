/**
 * Copyright (c) 2012 - 2022 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.emftriple.jena.helper;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.apache.jena.rdf.model.Literal;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;

/**
 * 
 * @author ilenia
 * @since Aug 23, 2022
 */
public class EcoreTTLHelper {
	public static EObject create(EClass eClass) {
		return EcoreUtil.create(eClass);
	}

	public static URI URI(EObject eObject) {
		return EcoreUtil.getURI(eObject);
	}

	public static String toStringURI(EObject eObject) {
		return toStringURI(URI(eObject));
	}

	public static String toStringURI(URI uri) {
		return uri.toString();		
	}

	public static Resource getResource(EObject eObject, Model graph) {
		return graph.getResource(toStringURI(eObject));
	}

	public static Property getProperty(EStructuralFeature eObject, Model graph) {
		return graph.getProperty(toStringURI(eObject));
	}
	
	public static Literal getLiteral(Object value, EAttribute attribute, Model graph) {
		String stringValue = EcoreUtil.convertToString(attribute.getEAttributeType(), value);
		return graph.createLiteral(stringValue);
	}
	
	public static EObject getEObject(ResourceSet resourceSet, Resource res) {
		URI eURI = URI.createURI(res.getURI());
		return resourceSet.getEObject(eURI, true);
	}
}
