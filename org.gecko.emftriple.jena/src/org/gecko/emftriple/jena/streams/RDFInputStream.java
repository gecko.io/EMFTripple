/**
 * Copyright (c) 2012 - 2022 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.emftriple.jena.streams;

import java.io.IOException;
import java.io.InputStream;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.URIConverter.Loadable;

/**
 * 
 * @author ilenia
 * @since Aug 23, 2022
 */
public class RDFInputStream extends InputStream implements Loadable {

	protected final URI uri;

	public RDFInputStream(URI uri) {
		this.uri = uri;
	}

	@Override
	public int read() throws IOException {
		return 0;
	}

	@Override
	public void loadResource(Resource resource) throws IOException {}
}
