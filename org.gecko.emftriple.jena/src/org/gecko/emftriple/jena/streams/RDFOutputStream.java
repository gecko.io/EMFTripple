/**
 * Copyright (c) 2012 - 2022 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.emftriple.jena.streams;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.URIConverter.Saveable;
import org.gecko.emftriple.jena.map.EObjectMapper;

/**
 * 
 * @author ilenia
 * @since Aug 23, 2022
 */
public class RDFOutputStream extends ByteArrayOutputStream implements Saveable {

	protected final URI uri;
	protected Model model;

	public RDFOutputStream(URI uri) {
		this.uri = uri;
	}

	@Override 
	public void saveResource(Resource resource) throws IOException {
		EObjectMapper mapper = new EObjectMapper();
		this.model = ModelFactory.createDefaultModel();
		mapper.to(
			model,
			resource
		);
	}

	@Override 
	public void close() throws IOException {
		if (model != null) {
			// do something
		}
	}

}
