/**
 */
package org.gecko.smartmodels.rdf.model.rdf;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Resource</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.gecko.smartmodels.rdf.model.rdf.Resource#getGraph <em>Graph</em>}</li>
 * </ul>
 *
 * @see org.gecko.smartmodels.rdf.model.rdf.RDFPackage#getResource()
 * @model
 * @generated
 */
public interface Resource extends SubjectNode, URIElement {
	/**
	 * Returns the value of the '<em><b>Graph</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link org.gecko.smartmodels.rdf.model.rdf.RDFGraph#getResources <em>Resources</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Graph</em>' container reference.
	 * @see #setGraph(RDFGraph)
	 * @see org.gecko.smartmodels.rdf.model.rdf.RDFPackage#getResource_Graph()
	 * @see org.gecko.smartmodels.rdf.model.rdf.RDFGraph#getResources
	 * @model opposite="resources"
	 * @generated
	 */
	RDFGraph getGraph();

	/**
	 * Sets the value of the '{@link org.gecko.smartmodels.rdf.model.rdf.Resource#getGraph <em>Graph</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Graph</em>' container reference.
	 * @see #getGraph()
	 * @generated
	 */
	void setGraph(RDFGraph value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean isTypeOf(String uri);

} // Resource
