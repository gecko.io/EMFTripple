/**
 */
package org.gecko.smartmodels.rdf.model.rdf.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.gecko.smartmodels.rdf.model.rdf.RDFPackage;
import org.gecko.smartmodels.rdf.model.rdf.Triple;
import org.gecko.smartmodels.rdf.model.rdf.TripleNode;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Triple Node</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.gecko.smartmodels.rdf.model.rdf.impl.TripleNodeImpl#getTriple <em>Triple</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TripleNodeImpl extends ResourceImpl implements TripleNode {
	/**
	 * The cached value of the '{@link #getTriple() <em>Triple</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTriple()
	 * @generated
	 * @ordered
	 */
	protected Triple triple;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TripleNodeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RDFPackage.Literals.TRIPLE_NODE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Triple getTriple() {
		if (triple != null && triple.eIsProxy()) {
			InternalEObject oldTriple = (InternalEObject)triple;
			triple = (Triple)eResolveProxy(oldTriple);
			if (triple != oldTriple) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, RDFPackage.TRIPLE_NODE__TRIPLE, oldTriple, triple));
			}
		}
		return triple;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Triple basicGetTriple() {
		return triple;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setTriple(Triple newTriple) {
		Triple oldTriple = triple;
		triple = newTriple;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RDFPackage.TRIPLE_NODE__TRIPLE, oldTriple, triple));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case RDFPackage.TRIPLE_NODE__TRIPLE:
				if (resolve) return getTriple();
				return basicGetTriple();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case RDFPackage.TRIPLE_NODE__TRIPLE:
				setTriple((Triple)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case RDFPackage.TRIPLE_NODE__TRIPLE:
				setTriple((Triple)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case RDFPackage.TRIPLE_NODE__TRIPLE:
				return triple != null;
		}
		return super.eIsSet(featureID);
	}

} //TripleNodeImpl
