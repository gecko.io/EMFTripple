/**
 */
package org.gecko.smartmodels.rdf.model.rdf;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Datatype</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.gecko.smartmodels.rdf.model.rdf.RDFPackage#getDatatype()
 * @model
 * @generated
 */
public interface Datatype extends Resource {
} // Datatype
