/**
 */
package org.gecko.smartmodels.rdf.model.rdf;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Alt</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.gecko.smartmodels.rdf.model.rdf.RDFAlt#getElements <em>Elements</em>}</li>
 * </ul>
 *
 * @see org.gecko.smartmodels.rdf.model.rdf.RDFPackage#getRDFAlt()
 * @model
 * @generated
 */
public interface RDFAlt extends RDFSContainer {
	/**
	 * Returns the value of the '<em><b>Elements</b></em>' reference list.
	 * The list contents are of type {@link org.gecko.smartmodels.rdf.model.rdf.Node}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Elements</em>' reference list.
	 * @see org.gecko.smartmodels.rdf.model.rdf.RDFPackage#getRDFAlt_Elements()
	 * @model
	 * @generated
	 */
	EList<Node> getElements();

} // RDFAlt
