/**
 */
package org.gecko.smartmodels.rdf.model.rdf;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Property</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.gecko.smartmodels.rdf.model.rdf.Property#getPredicateOf <em>Predicate Of</em>}</li>
 * </ul>
 *
 * @see org.gecko.smartmodels.rdf.model.rdf.RDFPackage#getProperty()
 * @model
 * @generated
 */
public interface Property extends Resource {
	/**
	 * Returns the value of the '<em><b>Predicate Of</b></em>' reference list.
	 * The list contents are of type {@link org.gecko.smartmodels.rdf.model.rdf.Triple}.
	 * It is bidirectional and its opposite is '{@link org.gecko.smartmodels.rdf.model.rdf.Triple#getPredicate <em>Predicate</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Predicate Of</em>' reference list.
	 * @see org.gecko.smartmodels.rdf.model.rdf.RDFPackage#getProperty_PredicateOf()
	 * @see org.gecko.smartmodels.rdf.model.rdf.Triple#getPredicate
	 * @model opposite="predicate"
	 * @generated
	 */
	EList<Triple> getPredicateOf();

} // Property
