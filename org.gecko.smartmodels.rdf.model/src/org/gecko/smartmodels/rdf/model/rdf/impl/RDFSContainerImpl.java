/**
 */
package org.gecko.smartmodels.rdf.model.rdf.impl;

import org.eclipse.emf.ecore.EClass;

import org.gecko.smartmodels.rdf.model.rdf.RDFPackage;
import org.gecko.smartmodels.rdf.model.rdf.RDFSContainer;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>SContainer</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class RDFSContainerImpl extends BlankNodeImpl implements RDFSContainer {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RDFSContainerImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RDFPackage.Literals.RDF_SCONTAINER;
	}

} //RDFSContainerImpl
