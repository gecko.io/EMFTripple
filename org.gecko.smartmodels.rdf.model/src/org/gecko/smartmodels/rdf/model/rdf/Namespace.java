/**
 */
package org.gecko.smartmodels.rdf.model.rdf;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Namespace</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.gecko.smartmodels.rdf.model.rdf.Namespace#getPrefix <em>Prefix</em>}</li>
 *   <li>{@link org.gecko.smartmodels.rdf.model.rdf.Namespace#getGraph <em>Graph</em>}</li>
 * </ul>
 *
 * @see org.gecko.smartmodels.rdf.model.rdf.RDFPackage#getNamespace()
 * @model
 * @generated
 */
public interface Namespace extends URIElement {
	/**
	 * Returns the value of the '<em><b>Prefix</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Prefix</em>' attribute.
	 * @see #setPrefix(String)
	 * @see org.gecko.smartmodels.rdf.model.rdf.RDFPackage#getNamespace_Prefix()
	 * @model required="true"
	 * @generated
	 */
	String getPrefix();

	/**
	 * Sets the value of the '{@link org.gecko.smartmodels.rdf.model.rdf.Namespace#getPrefix <em>Prefix</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Prefix</em>' attribute.
	 * @see #getPrefix()
	 * @generated
	 */
	void setPrefix(String value);

	/**
	 * Returns the value of the '<em><b>Graph</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link org.gecko.smartmodels.rdf.model.rdf.DocumentGraph#getNamespaces <em>Namespaces</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Graph</em>' container reference.
	 * @see #setGraph(DocumentGraph)
	 * @see org.gecko.smartmodels.rdf.model.rdf.RDFPackage#getNamespace_Graph()
	 * @see org.gecko.smartmodels.rdf.model.rdf.DocumentGraph#getNamespaces
	 * @model opposite="namespaces" required="true" transient="false"
	 * @generated
	 */
	DocumentGraph getGraph();

	/**
	 * Sets the value of the '{@link org.gecko.smartmodels.rdf.model.rdf.Namespace#getGraph <em>Graph</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Graph</em>' container reference.
	 * @see #getGraph()
	 * @generated
	 */
	void setGraph(DocumentGraph value);

} // Namespace
