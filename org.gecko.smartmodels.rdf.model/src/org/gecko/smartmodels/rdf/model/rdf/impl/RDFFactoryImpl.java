/**
 */
package org.gecko.smartmodels.rdf.model.rdf.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.gecko.smartmodels.rdf.model.rdf.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class RDFFactoryImpl extends EFactoryImpl implements RDFFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static RDFFactory init() {
		try {
			RDFFactory theRDFFactory = (RDFFactory)EPackage.Registry.INSTANCE.getEFactory(RDFPackage.eNS_URI);
			if (theRDFFactory != null) {
				return theRDFFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new RDFFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RDFFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case RDFPackage.NAMESPACE: return createNamespace();
			case RDFPackage.DOCUMENT_GRAPH: return createDocumentGraph();
			case RDFPackage.TRIPLE: return createTriple();
			case RDFPackage.NAMED_GRAPH: return createNamedGraph();
			case RDFPackage.BLANK_NODE: return createBlankNode();
			case RDFPackage.TRIPLE_NODE: return createTripleNode();
			case RDFPackage.RESOURCE: return createResource();
			case RDFPackage.PROPERTY: return createProperty();
			case RDFPackage.DATATYPE: return createDatatype();
			case RDFPackage.LITERAL: return createLiteral();
			case RDFPackage.RDF_BAG: return createRDFBag();
			case RDFPackage.RDF_ALT: return createRDFAlt();
			case RDFPackage.RDF_SEQ: return createRDFSeq();
			case RDFPackage.RDF_LIST: return createRDFList();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Namespace createNamespace() {
		NamespaceImpl namespace = new NamespaceImpl();
		return namespace;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DocumentGraph createDocumentGraph() {
		DocumentGraphImpl documentGraph = new DocumentGraphImpl();
		return documentGraph;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Triple createTriple() {
		TripleImpl triple = new TripleImpl();
		return triple;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NamedGraph createNamedGraph() {
		NamedGraphImpl namedGraph = new NamedGraphImpl();
		return namedGraph;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public BlankNode createBlankNode() {
		BlankNodeImpl blankNode = new BlankNodeImpl();
		return blankNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public TripleNode createTripleNode() {
		TripleNodeImpl tripleNode = new TripleNodeImpl();
		return tripleNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Resource createResource() {
		ResourceImpl resource = new ResourceImpl();
		return resource;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Property createProperty() {
		PropertyImpl property = new PropertyImpl();
		return property;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Datatype createDatatype() {
		DatatypeImpl datatype = new DatatypeImpl();
		return datatype;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Literal createLiteral() {
		LiteralImpl literal = new LiteralImpl();
		return literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public RDFBag createRDFBag() {
		RDFBagImpl rdfBag = new RDFBagImpl();
		return rdfBag;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public RDFAlt createRDFAlt() {
		RDFAltImpl rdfAlt = new RDFAltImpl();
		return rdfAlt;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public RDFSeq createRDFSeq() {
		RDFSeqImpl rdfSeq = new RDFSeqImpl();
		return rdfSeq;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public RDFList createRDFList() {
		RDFListImpl rdfList = new RDFListImpl();
		return rdfList;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public RDFPackage getRDFPackage() {
		return (RDFPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static RDFPackage getPackage() {
		return RDFPackage.eINSTANCE;
	}

} //RDFFactoryImpl
