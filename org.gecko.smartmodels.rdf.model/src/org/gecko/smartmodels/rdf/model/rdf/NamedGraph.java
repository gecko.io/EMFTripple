/**
 */
package org.gecko.smartmodels.rdf.model.rdf;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Named Graph</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.gecko.smartmodels.rdf.model.rdf.NamedGraph#getDocument <em>Document</em>}</li>
 * </ul>
 *
 * @see org.gecko.smartmodels.rdf.model.rdf.RDFPackage#getNamedGraph()
 * @model
 * @generated
 */
public interface NamedGraph extends RDFGraph, Node {
	/**
	 * Returns the value of the '<em><b>Document</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link org.gecko.smartmodels.rdf.model.rdf.DocumentGraph#getSubGraphs <em>Sub Graphs</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Document</em>' reference.
	 * @see #setDocument(DocumentGraph)
	 * @see org.gecko.smartmodels.rdf.model.rdf.RDFPackage#getNamedGraph_Document()
	 * @see org.gecko.smartmodels.rdf.model.rdf.DocumentGraph#getSubGraphs
	 * @model opposite="subGraphs" required="true"
	 * @generated
	 */
	DocumentGraph getDocument();

	/**
	 * Sets the value of the '{@link org.gecko.smartmodels.rdf.model.rdf.NamedGraph#getDocument <em>Document</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Document</em>' reference.
	 * @see #getDocument()
	 * @generated
	 */
	void setDocument(DocumentGraph value);

} // NamedGraph
