/**
 */
package org.gecko.smartmodels.rdf.model.rdf;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>SContainer</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.gecko.smartmodels.rdf.model.rdf.RDFPackage#getRDFSContainer()
 * @model abstract="true"
 * @generated
 */
public interface RDFSContainer extends BlankNode {
} // RDFSContainer
