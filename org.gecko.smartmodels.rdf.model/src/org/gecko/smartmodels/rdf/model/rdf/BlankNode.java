/**
 */
package org.gecko.smartmodels.rdf.model.rdf;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Blank Node</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.gecko.smartmodels.rdf.model.rdf.BlankNode#getNodeID <em>Node ID</em>}</li>
 *   <li>{@link org.gecko.smartmodels.rdf.model.rdf.BlankNode#getGraph <em>Graph</em>}</li>
 * </ul>
 *
 * @see org.gecko.smartmodels.rdf.model.rdf.RDFPackage#getBlankNode()
 * @model
 * @generated
 */
public interface BlankNode extends SubjectNode {
	/**
	 * Returns the value of the '<em><b>Node ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Node ID</em>' attribute.
	 * @see #setNodeID(String)
	 * @see org.gecko.smartmodels.rdf.model.rdf.RDFPackage#getBlankNode_NodeID()
	 * @model id="true" required="true"
	 * @generated
	 */
	String getNodeID();

	/**
	 * Sets the value of the '{@link org.gecko.smartmodels.rdf.model.rdf.BlankNode#getNodeID <em>Node ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Node ID</em>' attribute.
	 * @see #getNodeID()
	 * @generated
	 */
	void setNodeID(String value);

	/**
	 * Returns the value of the '<em><b>Graph</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link org.gecko.smartmodels.rdf.model.rdf.RDFGraph#getBlankNodes <em>Blank Nodes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Graph</em>' container reference.
	 * @see #setGraph(RDFGraph)
	 * @see org.gecko.smartmodels.rdf.model.rdf.RDFPackage#getBlankNode_Graph()
	 * @see org.gecko.smartmodels.rdf.model.rdf.RDFGraph#getBlankNodes
	 * @model opposite="blankNodes" transient="false"
	 * @generated
	 */
	RDFGraph getGraph();

	/**
	 * Sets the value of the '{@link org.gecko.smartmodels.rdf.model.rdf.BlankNode#getGraph <em>Graph</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Graph</em>' container reference.
	 * @see #getGraph()
	 * @generated
	 */
	void setGraph(RDFGraph value);

} // BlankNode
