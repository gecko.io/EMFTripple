/**
 */
package org.gecko.smartmodels.rdf.model.rdf;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Document Graph</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.gecko.smartmodels.rdf.model.rdf.DocumentGraph#getNamespaces <em>Namespaces</em>}</li>
 *   <li>{@link org.gecko.smartmodels.rdf.model.rdf.DocumentGraph#getSubGraphs <em>Sub Graphs</em>}</li>
 * </ul>
 *
 * @see org.gecko.smartmodels.rdf.model.rdf.RDFPackage#getDocumentGraph()
 * @model
 * @generated
 */
public interface DocumentGraph extends RDFGraph {
	/**
	 * Returns the value of the '<em><b>Namespaces</b></em>' containment reference list.
	 * The list contents are of type {@link org.gecko.smartmodels.rdf.model.rdf.Namespace}.
	 * It is bidirectional and its opposite is '{@link org.gecko.smartmodels.rdf.model.rdf.Namespace#getGraph <em>Graph</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Namespaces</em>' containment reference list.
	 * @see org.gecko.smartmodels.rdf.model.rdf.RDFPackage#getDocumentGraph_Namespaces()
	 * @see org.gecko.smartmodels.rdf.model.rdf.Namespace#getGraph
	 * @model opposite="graph" containment="true"
	 * @generated
	 */
	EList<Namespace> getNamespaces();

	/**
	 * Returns the value of the '<em><b>Sub Graphs</b></em>' reference list.
	 * The list contents are of type {@link org.gecko.smartmodels.rdf.model.rdf.NamedGraph}.
	 * It is bidirectional and its opposite is '{@link org.gecko.smartmodels.rdf.model.rdf.NamedGraph#getDocument <em>Document</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sub Graphs</em>' reference list.
	 * @see org.gecko.smartmodels.rdf.model.rdf.RDFPackage#getDocumentGraph_SubGraphs()
	 * @see org.gecko.smartmodels.rdf.model.rdf.NamedGraph#getDocument
	 * @model opposite="document"
	 * @generated
	 */
	EList<NamedGraph> getSubGraphs();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true"
	 * @generated
	 */
	NamedGraph getNamedGraph(String uri);

} // DocumentGraph
