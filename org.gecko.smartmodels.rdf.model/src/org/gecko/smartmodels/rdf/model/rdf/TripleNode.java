/**
 */
package org.gecko.smartmodels.rdf.model.rdf;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Triple Node</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.gecko.smartmodels.rdf.model.rdf.TripleNode#getTriple <em>Triple</em>}</li>
 * </ul>
 *
 * @see org.gecko.smartmodels.rdf.model.rdf.RDFPackage#getTripleNode()
 * @model
 * @generated
 */
public interface TripleNode extends Resource {
	/**
	 * Returns the value of the '<em><b>Triple</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Triple</em>' reference.
	 * @see #setTriple(Triple)
	 * @see org.gecko.smartmodels.rdf.model.rdf.RDFPackage#getTripleNode_Triple()
	 * @model required="true"
	 * @generated
	 */
	Triple getTriple();

	/**
	 * Sets the value of the '{@link org.gecko.smartmodels.rdf.model.rdf.TripleNode#getTriple <em>Triple</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Triple</em>' reference.
	 * @see #getTriple()
	 * @generated
	 */
	void setTriple(Triple value);

} // TripleNode
