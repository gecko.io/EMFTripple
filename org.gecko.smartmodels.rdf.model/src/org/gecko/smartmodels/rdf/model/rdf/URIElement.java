/**
 */
package org.gecko.smartmodels.rdf.model.rdf;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>URI Element</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.gecko.smartmodels.rdf.model.rdf.URIElement#getURI <em>URI</em>}</li>
 *   <li>{@link org.gecko.smartmodels.rdf.model.rdf.URIElement#getNamespace <em>Namespace</em>}</li>
 * </ul>
 *
 * @see org.gecko.smartmodels.rdf.model.rdf.RDFPackage#getURIElement()
 * @model abstract="true"
 * @generated
 */
public interface URIElement extends EObject {
	/**
	 * Returns the value of the '<em><b>URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>URI</em>' attribute.
	 * @see #setURI(String)
	 * @see org.gecko.smartmodels.rdf.model.rdf.RDFPackage#getURIElement_URI()
	 * @model id="true" required="true"
	 *        annotation="Id base=''"
	 * @generated
	 */
	String getURI();

	/**
	 * Sets the value of the '{@link org.gecko.smartmodels.rdf.model.rdf.URIElement#getURI <em>URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>URI</em>' attribute.
	 * @see #getURI()
	 * @generated
	 */
	void setURI(String value);

	/**
	 * Returns the value of the '<em><b>Namespace</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Namespace</em>' reference.
	 * @see #setNamespace(Namespace)
	 * @see org.gecko.smartmodels.rdf.model.rdf.RDFPackage#getURIElement_Namespace()
	 * @model
	 * @generated
	 */
	Namespace getNamespace();

	/**
	 * Sets the value of the '{@link org.gecko.smartmodels.rdf.model.rdf.URIElement#getNamespace <em>Namespace</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Namespace</em>' reference.
	 * @see #getNamespace()
	 * @generated
	 */
	void setNamespace(Namespace value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 * @generated
	 */
	String getLocalName();

} // URIElement
