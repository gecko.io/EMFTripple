/**
 */
package org.gecko.smartmodels.rdf.model.rdf.impl;

import java.lang.reflect.InvocationTargetException;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.util.BasicFeatureMap;
import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.FeatureMap;
import org.eclipse.emf.ecore.util.InternalEList;

import org.gecko.smartmodels.rdf.model.rdf.BlankNode;
import org.gecko.smartmodels.rdf.model.rdf.Datatype;
import org.gecko.smartmodels.rdf.model.rdf.Literal;
import org.gecko.smartmodels.rdf.model.rdf.Node;
import org.gecko.smartmodels.rdf.model.rdf.Property;
import org.gecko.smartmodels.rdf.model.rdf.RDFGraph;
import org.gecko.smartmodels.rdf.model.rdf.RDFPackage;
import org.gecko.smartmodels.rdf.model.rdf.Resource;
import org.gecko.smartmodels.rdf.model.rdf.SubjectNode;
import org.gecko.smartmodels.rdf.model.rdf.Triple;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Graph</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.gecko.smartmodels.rdf.model.rdf.impl.RDFGraphImpl#getNodes <em>Nodes</em>}</li>
 *   <li>{@link org.gecko.smartmodels.rdf.model.rdf.impl.RDFGraphImpl#getResources <em>Resources</em>}</li>
 *   <li>{@link org.gecko.smartmodels.rdf.model.rdf.impl.RDFGraphImpl#getProperties <em>Properties</em>}</li>
 *   <li>{@link org.gecko.smartmodels.rdf.model.rdf.impl.RDFGraphImpl#getBlankNodes <em>Blank Nodes</em>}</li>
 *   <li>{@link org.gecko.smartmodels.rdf.model.rdf.impl.RDFGraphImpl#getDatatypes <em>Datatypes</em>}</li>
 *   <li>{@link org.gecko.smartmodels.rdf.model.rdf.impl.RDFGraphImpl#getLiterals <em>Literals</em>}</li>
 *   <li>{@link org.gecko.smartmodels.rdf.model.rdf.impl.RDFGraphImpl#getTriples <em>Triples</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class RDFGraphImpl extends URIElementImpl implements RDFGraph {
	/**
	 * The cached value of the '{@link #getNodes() <em>Nodes</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNodes()
	 * @generated
	 * @ordered
	 */
	protected FeatureMap nodes;

	/**
	 * The cached value of the '{@link #getTriples() <em>Triples</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTriples()
	 * @generated
	 * @ordered
	 */
	protected EList<Triple> triples;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RDFGraphImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RDFPackage.Literals.RDF_GRAPH;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public FeatureMap getNodes() {
		if (nodes == null) {
			nodes = new BasicFeatureMap(this, RDFPackage.RDF_GRAPH__NODES);
		}
		return nodes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Resource> getResources() {
		return getNodes().list(RDFPackage.Literals.RDF_GRAPH__RESOURCES);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Property> getProperties() {
		return getNodes().list(RDFPackage.Literals.RDF_GRAPH__PROPERTIES);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<BlankNode> getBlankNodes() {
		return getNodes().list(RDFPackage.Literals.RDF_GRAPH__BLANK_NODES);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Datatype> getDatatypes() {
		return getNodes().list(RDFPackage.Literals.RDF_GRAPH__DATATYPES);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Literal> getLiterals() {
		return getNodes().list(RDFPackage.Literals.RDF_GRAPH__LITERALS);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Triple> getTriples() {
		if (triples == null) {
			triples = new EObjectContainmentWithInverseEList<Triple>(Triple.class, this, RDFPackage.RDF_GRAPH__TRIPLES, RDFPackage.TRIPLE__GRAPH);
		}
		return triples;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isEquivalentTo(RDFGraph graph) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void add(RDFGraph graph) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public RDFGraph union(RDFGraph graph) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public RDFGraph diff(RDFGraph graph) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Triple addTriple(Node subject, Property property, Node object) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<SubjectNode> listSubjectsWithProperty(Property property) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<SubjectNode> listSubjectsWithPropertyValue(Property property, Node object) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<SubjectNode> listSubjects() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Resource getResource(String uri) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Property getProperty(String uri) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Datatype getDatatype(String uri) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public BlankNode getBlankNode(String nodeID) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Triple> listAllTriples() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Resource> listAllResources() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Property> listAllProperties() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case RDFPackage.RDF_GRAPH__RESOURCES:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getResources()).basicAdd(otherEnd, msgs);
			case RDFPackage.RDF_GRAPH__BLANK_NODES:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getBlankNodes()).basicAdd(otherEnd, msgs);
			case RDFPackage.RDF_GRAPH__LITERALS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getLiterals()).basicAdd(otherEnd, msgs);
			case RDFPackage.RDF_GRAPH__TRIPLES:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getTriples()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case RDFPackage.RDF_GRAPH__NODES:
				return ((InternalEList<?>)getNodes()).basicRemove(otherEnd, msgs);
			case RDFPackage.RDF_GRAPH__RESOURCES:
				return ((InternalEList<?>)getResources()).basicRemove(otherEnd, msgs);
			case RDFPackage.RDF_GRAPH__PROPERTIES:
				return ((InternalEList<?>)getProperties()).basicRemove(otherEnd, msgs);
			case RDFPackage.RDF_GRAPH__BLANK_NODES:
				return ((InternalEList<?>)getBlankNodes()).basicRemove(otherEnd, msgs);
			case RDFPackage.RDF_GRAPH__DATATYPES:
				return ((InternalEList<?>)getDatatypes()).basicRemove(otherEnd, msgs);
			case RDFPackage.RDF_GRAPH__LITERALS:
				return ((InternalEList<?>)getLiterals()).basicRemove(otherEnd, msgs);
			case RDFPackage.RDF_GRAPH__TRIPLES:
				return ((InternalEList<?>)getTriples()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case RDFPackage.RDF_GRAPH__NODES:
				if (coreType) return getNodes();
				return ((FeatureMap.Internal)getNodes()).getWrapper();
			case RDFPackage.RDF_GRAPH__RESOURCES:
				return getResources();
			case RDFPackage.RDF_GRAPH__PROPERTIES:
				return getProperties();
			case RDFPackage.RDF_GRAPH__BLANK_NODES:
				return getBlankNodes();
			case RDFPackage.RDF_GRAPH__DATATYPES:
				return getDatatypes();
			case RDFPackage.RDF_GRAPH__LITERALS:
				return getLiterals();
			case RDFPackage.RDF_GRAPH__TRIPLES:
				return getTriples();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case RDFPackage.RDF_GRAPH__NODES:
				((FeatureMap.Internal)getNodes()).set(newValue);
				return;
			case RDFPackage.RDF_GRAPH__RESOURCES:
				getResources().clear();
				getResources().addAll((Collection<? extends Resource>)newValue);
				return;
			case RDFPackage.RDF_GRAPH__PROPERTIES:
				getProperties().clear();
				getProperties().addAll((Collection<? extends Property>)newValue);
				return;
			case RDFPackage.RDF_GRAPH__BLANK_NODES:
				getBlankNodes().clear();
				getBlankNodes().addAll((Collection<? extends BlankNode>)newValue);
				return;
			case RDFPackage.RDF_GRAPH__DATATYPES:
				getDatatypes().clear();
				getDatatypes().addAll((Collection<? extends Datatype>)newValue);
				return;
			case RDFPackage.RDF_GRAPH__LITERALS:
				getLiterals().clear();
				getLiterals().addAll((Collection<? extends Literal>)newValue);
				return;
			case RDFPackage.RDF_GRAPH__TRIPLES:
				getTriples().clear();
				getTriples().addAll((Collection<? extends Triple>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case RDFPackage.RDF_GRAPH__NODES:
				getNodes().clear();
				return;
			case RDFPackage.RDF_GRAPH__RESOURCES:
				getResources().clear();
				return;
			case RDFPackage.RDF_GRAPH__PROPERTIES:
				getProperties().clear();
				return;
			case RDFPackage.RDF_GRAPH__BLANK_NODES:
				getBlankNodes().clear();
				return;
			case RDFPackage.RDF_GRAPH__DATATYPES:
				getDatatypes().clear();
				return;
			case RDFPackage.RDF_GRAPH__LITERALS:
				getLiterals().clear();
				return;
			case RDFPackage.RDF_GRAPH__TRIPLES:
				getTriples().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case RDFPackage.RDF_GRAPH__NODES:
				return nodes != null && !nodes.isEmpty();
			case RDFPackage.RDF_GRAPH__RESOURCES:
				return !getResources().isEmpty();
			case RDFPackage.RDF_GRAPH__PROPERTIES:
				return !getProperties().isEmpty();
			case RDFPackage.RDF_GRAPH__BLANK_NODES:
				return !getBlankNodes().isEmpty();
			case RDFPackage.RDF_GRAPH__DATATYPES:
				return !getDatatypes().isEmpty();
			case RDFPackage.RDF_GRAPH__LITERALS:
				return !getLiterals().isEmpty();
			case RDFPackage.RDF_GRAPH__TRIPLES:
				return triples != null && !triples.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case RDFPackage.RDF_GRAPH___IS_EQUIVALENT_TO__RDFGRAPH:
				return isEquivalentTo((RDFGraph)arguments.get(0));
			case RDFPackage.RDF_GRAPH___ADD__RDFGRAPH:
				add((RDFGraph)arguments.get(0));
				return null;
			case RDFPackage.RDF_GRAPH___UNION__RDFGRAPH:
				return union((RDFGraph)arguments.get(0));
			case RDFPackage.RDF_GRAPH___DIFF__RDFGRAPH:
				return diff((RDFGraph)arguments.get(0));
			case RDFPackage.RDF_GRAPH___ADD_TRIPLE__NODE_PROPERTY_NODE:
				return addTriple((Node)arguments.get(0), (Property)arguments.get(1), (Node)arguments.get(2));
			case RDFPackage.RDF_GRAPH___LIST_SUBJECTS_WITH_PROPERTY__PROPERTY:
				return listSubjectsWithProperty((Property)arguments.get(0));
			case RDFPackage.RDF_GRAPH___LIST_SUBJECTS_WITH_PROPERTY_VALUE__PROPERTY_NODE:
				return listSubjectsWithPropertyValue((Property)arguments.get(0), (Node)arguments.get(1));
			case RDFPackage.RDF_GRAPH___LIST_SUBJECTS:
				return listSubjects();
			case RDFPackage.RDF_GRAPH___GET_RESOURCE__STRING:
				return getResource((String)arguments.get(0));
			case RDFPackage.RDF_GRAPH___GET_PROPERTY__STRING:
				return getProperty((String)arguments.get(0));
			case RDFPackage.RDF_GRAPH___GET_DATATYPE__STRING:
				return getDatatype((String)arguments.get(0));
			case RDFPackage.RDF_GRAPH___GET_BLANK_NODE__STRING:
				return getBlankNode((String)arguments.get(0));
			case RDFPackage.RDF_GRAPH___LIST_ALL_TRIPLES:
				return listAllTriples();
			case RDFPackage.RDF_GRAPH___LIST_ALL_RESOURCES:
				return listAllResources();
			case RDFPackage.RDF_GRAPH___LIST_ALL_PROPERTIES:
				return listAllProperties();
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (nodes: ");
		result.append(nodes);
		result.append(')');
		return result.toString();
	}

} //RDFGraphImpl
