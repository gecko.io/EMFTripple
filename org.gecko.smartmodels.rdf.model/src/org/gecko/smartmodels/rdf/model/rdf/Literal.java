/**
 */
package org.gecko.smartmodels.rdf.model.rdf;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Literal</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.gecko.smartmodels.rdf.model.rdf.Literal#getLexicalForm <em>Lexical Form</em>}</li>
 *   <li>{@link org.gecko.smartmodels.rdf.model.rdf.Literal#getLang <em>Lang</em>}</li>
 *   <li>{@link org.gecko.smartmodels.rdf.model.rdf.Literal#getDatatype <em>Datatype</em>}</li>
 *   <li>{@link org.gecko.smartmodels.rdf.model.rdf.Literal#getGraph <em>Graph</em>}</li>
 * </ul>
 *
 * @see org.gecko.smartmodels.rdf.model.rdf.RDFPackage#getLiteral()
 * @model
 * @generated
 */
public interface Literal extends Node {
	/**
	 * Returns the value of the '<em><b>Lexical Form</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Lexical Form</em>' attribute.
	 * @see #setLexicalForm(String)
	 * @see org.gecko.smartmodels.rdf.model.rdf.RDFPackage#getLiteral_LexicalForm()
	 * @model required="true"
	 * @generated
	 */
	String getLexicalForm();

	/**
	 * Sets the value of the '{@link org.gecko.smartmodels.rdf.model.rdf.Literal#getLexicalForm <em>Lexical Form</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Lexical Form</em>' attribute.
	 * @see #getLexicalForm()
	 * @generated
	 */
	void setLexicalForm(String value);

	/**
	 * Returns the value of the '<em><b>Lang</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Lang</em>' attribute.
	 * @see #setLang(String)
	 * @see org.gecko.smartmodels.rdf.model.rdf.RDFPackage#getLiteral_Lang()
	 * @model
	 * @generated
	 */
	String getLang();

	/**
	 * Sets the value of the '{@link org.gecko.smartmodels.rdf.model.rdf.Literal#getLang <em>Lang</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Lang</em>' attribute.
	 * @see #getLang()
	 * @generated
	 */
	void setLang(String value);

	/**
	 * Returns the value of the '<em><b>Datatype</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Datatype</em>' reference.
	 * @see #setDatatype(Datatype)
	 * @see org.gecko.smartmodels.rdf.model.rdf.RDFPackage#getLiteral_Datatype()
	 * @model required="true"
	 * @generated
	 */
	Datatype getDatatype();

	/**
	 * Sets the value of the '{@link org.gecko.smartmodels.rdf.model.rdf.Literal#getDatatype <em>Datatype</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Datatype</em>' reference.
	 * @see #getDatatype()
	 * @generated
	 */
	void setDatatype(Datatype value);

	/**
	 * Returns the value of the '<em><b>Graph</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link org.gecko.smartmodels.rdf.model.rdf.RDFGraph#getLiterals <em>Literals</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Graph</em>' container reference.
	 * @see #setGraph(RDFGraph)
	 * @see org.gecko.smartmodels.rdf.model.rdf.RDFPackage#getLiteral_Graph()
	 * @see org.gecko.smartmodels.rdf.model.rdf.RDFGraph#getLiterals
	 * @model opposite="literals" transient="false"
	 * @generated
	 */
	RDFGraph getGraph();

	/**
	 * Sets the value of the '{@link org.gecko.smartmodels.rdf.model.rdf.Literal#getGraph <em>Graph</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Graph</em>' container reference.
	 * @see #getGraph()
	 * @generated
	 */
	void setGraph(RDFGraph value);

} // Literal
