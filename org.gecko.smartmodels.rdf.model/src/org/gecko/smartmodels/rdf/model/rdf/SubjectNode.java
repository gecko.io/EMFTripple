/**
 */
package org.gecko.smartmodels.rdf.model.rdf;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Subject Node</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.gecko.smartmodels.rdf.model.rdf.SubjectNode#getSubjectOf <em>Subject Of</em>}</li>
 * </ul>
 *
 * @see org.gecko.smartmodels.rdf.model.rdf.RDFPackage#getSubjectNode()
 * @model abstract="true"
 * @generated
 */
public interface SubjectNode extends Node {
	/**
	 * Returns the value of the '<em><b>Subject Of</b></em>' reference list.
	 * The list contents are of type {@link org.gecko.smartmodels.rdf.model.rdf.Triple}.
	 * It is bidirectional and its opposite is '{@link org.gecko.smartmodels.rdf.model.rdf.Triple#getSubject <em>Subject</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Subject Of</em>' reference list.
	 * @see org.gecko.smartmodels.rdf.model.rdf.RDFPackage#getSubjectNode_SubjectOf()
	 * @see org.gecko.smartmodels.rdf.model.rdf.Triple#getSubject
	 * @model opposite="subject"
	 * @generated
	 */
	EList<Triple> getSubjectOf();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean isTypeOf(String uri);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 * @generated
	 */
	EList<Resource> getTypes();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true"
	 * @generated
	 */
	String getStringValue(String uri);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	EList<Node> getValues(String uri);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	EList<String> getURIValues(String uri);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	EList<Node> getObject(String uri);

} // SubjectNode
