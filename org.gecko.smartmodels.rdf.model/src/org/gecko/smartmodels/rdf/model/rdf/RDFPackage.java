/**
 */
package org.gecko.smartmodels.rdf.model.rdf;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.gecko.smartmodels.rdf.model.rdf.RDFFactory
 * @model kind="package"
 * @generated
 */
public interface RDFPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "rdf";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.w3.org/1999/02/22-rdf-syntax-ns#";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "RDF";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	RDFPackage eINSTANCE = org.gecko.smartmodels.rdf.model.rdf.impl.RDFPackageImpl.init();

	/**
	 * The meta object id for the '{@link org.gecko.smartmodels.rdf.model.rdf.impl.URIElementImpl <em>URI Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.gecko.smartmodels.rdf.model.rdf.impl.URIElementImpl
	 * @see org.gecko.smartmodels.rdf.model.rdf.impl.RDFPackageImpl#getURIElement()
	 * @generated
	 */
	int URI_ELEMENT = 0;

	/**
	 * The feature id for the '<em><b>URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int URI_ELEMENT__URI = 0;

	/**
	 * The feature id for the '<em><b>Namespace</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int URI_ELEMENT__NAMESPACE = 1;

	/**
	 * The number of structural features of the '<em>URI Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int URI_ELEMENT_FEATURE_COUNT = 2;

	/**
	 * The operation id for the '<em>Get Local Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int URI_ELEMENT___GET_LOCAL_NAME = 0;

	/**
	 * The number of operations of the '<em>URI Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int URI_ELEMENT_OPERATION_COUNT = 1;

	/**
	 * The meta object id for the '{@link org.gecko.smartmodels.rdf.model.rdf.impl.NamespaceImpl <em>Namespace</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.gecko.smartmodels.rdf.model.rdf.impl.NamespaceImpl
	 * @see org.gecko.smartmodels.rdf.model.rdf.impl.RDFPackageImpl#getNamespace()
	 * @generated
	 */
	int NAMESPACE = 1;

	/**
	 * The feature id for the '<em><b>URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMESPACE__URI = URI_ELEMENT__URI;

	/**
	 * The feature id for the '<em><b>Namespace</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMESPACE__NAMESPACE = URI_ELEMENT__NAMESPACE;

	/**
	 * The feature id for the '<em><b>Prefix</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMESPACE__PREFIX = URI_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Graph</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMESPACE__GRAPH = URI_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Namespace</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMESPACE_FEATURE_COUNT = URI_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>Get Local Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMESPACE___GET_LOCAL_NAME = URI_ELEMENT___GET_LOCAL_NAME;

	/**
	 * The number of operations of the '<em>Namespace</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMESPACE_OPERATION_COUNT = URI_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.gecko.smartmodels.rdf.model.rdf.impl.RDFGraphImpl <em>Graph</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.gecko.smartmodels.rdf.model.rdf.impl.RDFGraphImpl
	 * @see org.gecko.smartmodels.rdf.model.rdf.impl.RDFPackageImpl#getRDFGraph()
	 * @generated
	 */
	int RDF_GRAPH = 2;

	/**
	 * The feature id for the '<em><b>URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RDF_GRAPH__URI = URI_ELEMENT__URI;

	/**
	 * The feature id for the '<em><b>Namespace</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RDF_GRAPH__NAMESPACE = URI_ELEMENT__NAMESPACE;

	/**
	 * The feature id for the '<em><b>Nodes</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RDF_GRAPH__NODES = URI_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Resources</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RDF_GRAPH__RESOURCES = URI_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RDF_GRAPH__PROPERTIES = URI_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Blank Nodes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RDF_GRAPH__BLANK_NODES = URI_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Datatypes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RDF_GRAPH__DATATYPES = URI_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Literals</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RDF_GRAPH__LITERALS = URI_ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Triples</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RDF_GRAPH__TRIPLES = URI_ELEMENT_FEATURE_COUNT + 6;

	/**
	 * The number of structural features of the '<em>Graph</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RDF_GRAPH_FEATURE_COUNT = URI_ELEMENT_FEATURE_COUNT + 7;

	/**
	 * The operation id for the '<em>Get Local Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RDF_GRAPH___GET_LOCAL_NAME = URI_ELEMENT___GET_LOCAL_NAME;

	/**
	 * The operation id for the '<em>Is Equivalent To</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RDF_GRAPH___IS_EQUIVALENT_TO__RDFGRAPH = URI_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Add</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RDF_GRAPH___ADD__RDFGRAPH = URI_ELEMENT_OPERATION_COUNT + 1;

	/**
	 * The operation id for the '<em>Union</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RDF_GRAPH___UNION__RDFGRAPH = URI_ELEMENT_OPERATION_COUNT + 2;

	/**
	 * The operation id for the '<em>Diff</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RDF_GRAPH___DIFF__RDFGRAPH = URI_ELEMENT_OPERATION_COUNT + 3;

	/**
	 * The operation id for the '<em>Add Triple</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RDF_GRAPH___ADD_TRIPLE__NODE_PROPERTY_NODE = URI_ELEMENT_OPERATION_COUNT + 4;

	/**
	 * The operation id for the '<em>List Subjects With Property</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RDF_GRAPH___LIST_SUBJECTS_WITH_PROPERTY__PROPERTY = URI_ELEMENT_OPERATION_COUNT + 5;

	/**
	 * The operation id for the '<em>List Subjects With Property Value</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RDF_GRAPH___LIST_SUBJECTS_WITH_PROPERTY_VALUE__PROPERTY_NODE = URI_ELEMENT_OPERATION_COUNT + 6;

	/**
	 * The operation id for the '<em>List Subjects</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RDF_GRAPH___LIST_SUBJECTS = URI_ELEMENT_OPERATION_COUNT + 7;

	/**
	 * The operation id for the '<em>Get Resource</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RDF_GRAPH___GET_RESOURCE__STRING = URI_ELEMENT_OPERATION_COUNT + 8;

	/**
	 * The operation id for the '<em>Get Property</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RDF_GRAPH___GET_PROPERTY__STRING = URI_ELEMENT_OPERATION_COUNT + 9;

	/**
	 * The operation id for the '<em>Get Datatype</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RDF_GRAPH___GET_DATATYPE__STRING = URI_ELEMENT_OPERATION_COUNT + 10;

	/**
	 * The operation id for the '<em>Get Blank Node</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RDF_GRAPH___GET_BLANK_NODE__STRING = URI_ELEMENT_OPERATION_COUNT + 11;

	/**
	 * The operation id for the '<em>List All Triples</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RDF_GRAPH___LIST_ALL_TRIPLES = URI_ELEMENT_OPERATION_COUNT + 12;

	/**
	 * The operation id for the '<em>List All Resources</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RDF_GRAPH___LIST_ALL_RESOURCES = URI_ELEMENT_OPERATION_COUNT + 13;

	/**
	 * The operation id for the '<em>List All Properties</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RDF_GRAPH___LIST_ALL_PROPERTIES = URI_ELEMENT_OPERATION_COUNT + 14;

	/**
	 * The number of operations of the '<em>Graph</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RDF_GRAPH_OPERATION_COUNT = URI_ELEMENT_OPERATION_COUNT + 15;

	/**
	 * The meta object id for the '{@link org.gecko.smartmodels.rdf.model.rdf.impl.DocumentGraphImpl <em>Document Graph</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.gecko.smartmodels.rdf.model.rdf.impl.DocumentGraphImpl
	 * @see org.gecko.smartmodels.rdf.model.rdf.impl.RDFPackageImpl#getDocumentGraph()
	 * @generated
	 */
	int DOCUMENT_GRAPH = 3;

	/**
	 * The feature id for the '<em><b>URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_GRAPH__URI = RDF_GRAPH__URI;

	/**
	 * The feature id for the '<em><b>Namespace</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_GRAPH__NAMESPACE = RDF_GRAPH__NAMESPACE;

	/**
	 * The feature id for the '<em><b>Nodes</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_GRAPH__NODES = RDF_GRAPH__NODES;

	/**
	 * The feature id for the '<em><b>Resources</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_GRAPH__RESOURCES = RDF_GRAPH__RESOURCES;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_GRAPH__PROPERTIES = RDF_GRAPH__PROPERTIES;

	/**
	 * The feature id for the '<em><b>Blank Nodes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_GRAPH__BLANK_NODES = RDF_GRAPH__BLANK_NODES;

	/**
	 * The feature id for the '<em><b>Datatypes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_GRAPH__DATATYPES = RDF_GRAPH__DATATYPES;

	/**
	 * The feature id for the '<em><b>Literals</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_GRAPH__LITERALS = RDF_GRAPH__LITERALS;

	/**
	 * The feature id for the '<em><b>Triples</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_GRAPH__TRIPLES = RDF_GRAPH__TRIPLES;

	/**
	 * The feature id for the '<em><b>Namespaces</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_GRAPH__NAMESPACES = RDF_GRAPH_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Sub Graphs</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_GRAPH__SUB_GRAPHS = RDF_GRAPH_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Document Graph</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_GRAPH_FEATURE_COUNT = RDF_GRAPH_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>Get Local Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_GRAPH___GET_LOCAL_NAME = RDF_GRAPH___GET_LOCAL_NAME;

	/**
	 * The operation id for the '<em>Is Equivalent To</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_GRAPH___IS_EQUIVALENT_TO__RDFGRAPH = RDF_GRAPH___IS_EQUIVALENT_TO__RDFGRAPH;

	/**
	 * The operation id for the '<em>Add</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_GRAPH___ADD__RDFGRAPH = RDF_GRAPH___ADD__RDFGRAPH;

	/**
	 * The operation id for the '<em>Union</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_GRAPH___UNION__RDFGRAPH = RDF_GRAPH___UNION__RDFGRAPH;

	/**
	 * The operation id for the '<em>Diff</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_GRAPH___DIFF__RDFGRAPH = RDF_GRAPH___DIFF__RDFGRAPH;

	/**
	 * The operation id for the '<em>Add Triple</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_GRAPH___ADD_TRIPLE__NODE_PROPERTY_NODE = RDF_GRAPH___ADD_TRIPLE__NODE_PROPERTY_NODE;

	/**
	 * The operation id for the '<em>List Subjects With Property</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_GRAPH___LIST_SUBJECTS_WITH_PROPERTY__PROPERTY = RDF_GRAPH___LIST_SUBJECTS_WITH_PROPERTY__PROPERTY;

	/**
	 * The operation id for the '<em>List Subjects With Property Value</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_GRAPH___LIST_SUBJECTS_WITH_PROPERTY_VALUE__PROPERTY_NODE = RDF_GRAPH___LIST_SUBJECTS_WITH_PROPERTY_VALUE__PROPERTY_NODE;

	/**
	 * The operation id for the '<em>List Subjects</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_GRAPH___LIST_SUBJECTS = RDF_GRAPH___LIST_SUBJECTS;

	/**
	 * The operation id for the '<em>Get Resource</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_GRAPH___GET_RESOURCE__STRING = RDF_GRAPH___GET_RESOURCE__STRING;

	/**
	 * The operation id for the '<em>Get Property</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_GRAPH___GET_PROPERTY__STRING = RDF_GRAPH___GET_PROPERTY__STRING;

	/**
	 * The operation id for the '<em>Get Datatype</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_GRAPH___GET_DATATYPE__STRING = RDF_GRAPH___GET_DATATYPE__STRING;

	/**
	 * The operation id for the '<em>Get Blank Node</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_GRAPH___GET_BLANK_NODE__STRING = RDF_GRAPH___GET_BLANK_NODE__STRING;

	/**
	 * The operation id for the '<em>List All Triples</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_GRAPH___LIST_ALL_TRIPLES = RDF_GRAPH___LIST_ALL_TRIPLES;

	/**
	 * The operation id for the '<em>List All Resources</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_GRAPH___LIST_ALL_RESOURCES = RDF_GRAPH___LIST_ALL_RESOURCES;

	/**
	 * The operation id for the '<em>List All Properties</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_GRAPH___LIST_ALL_PROPERTIES = RDF_GRAPH___LIST_ALL_PROPERTIES;

	/**
	 * The operation id for the '<em>Get Named Graph</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_GRAPH___GET_NAMED_GRAPH__STRING = RDF_GRAPH_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>Document Graph</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_GRAPH_OPERATION_COUNT = RDF_GRAPH_OPERATION_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.gecko.smartmodels.rdf.model.rdf.impl.TripleImpl <em>Triple</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.gecko.smartmodels.rdf.model.rdf.impl.TripleImpl
	 * @see org.gecko.smartmodels.rdf.model.rdf.impl.RDFPackageImpl#getTriple()
	 * @generated
	 */
	int TRIPLE = 4;

	/**
	 * The feature id for the '<em><b>Subject</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIPLE__SUBJECT = 0;

	/**
	 * The feature id for the '<em><b>Predicate</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIPLE__PREDICATE = 1;

	/**
	 * The feature id for the '<em><b>Object</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIPLE__OBJECT = 2;

	/**
	 * The feature id for the '<em><b>Graph</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIPLE__GRAPH = 3;

	/**
	 * The number of structural features of the '<em>Triple</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIPLE_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Triple</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIPLE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.gecko.smartmodels.rdf.model.rdf.impl.NodeImpl <em>Node</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.gecko.smartmodels.rdf.model.rdf.impl.NodeImpl
	 * @see org.gecko.smartmodels.rdf.model.rdf.impl.RDFPackageImpl#getNode()
	 * @generated
	 */
	int NODE = 5;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NODE__LABEL = 0;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NODE__COMMENT = 1;

	/**
	 * The feature id for the '<em><b>Object Of</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NODE__OBJECT_OF = 2;

	/**
	 * The number of structural features of the '<em>Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NODE_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NODE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.gecko.smartmodels.rdf.model.rdf.impl.SubjectNodeImpl <em>Subject Node</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.gecko.smartmodels.rdf.model.rdf.impl.SubjectNodeImpl
	 * @see org.gecko.smartmodels.rdf.model.rdf.impl.RDFPackageImpl#getSubjectNode()
	 * @generated
	 */
	int SUBJECT_NODE = 6;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBJECT_NODE__LABEL = NODE__LABEL;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBJECT_NODE__COMMENT = NODE__COMMENT;

	/**
	 * The feature id for the '<em><b>Object Of</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBJECT_NODE__OBJECT_OF = NODE__OBJECT_OF;

	/**
	 * The feature id for the '<em><b>Subject Of</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBJECT_NODE__SUBJECT_OF = NODE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Subject Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBJECT_NODE_FEATURE_COUNT = NODE_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Is Type Of</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBJECT_NODE___IS_TYPE_OF__STRING = NODE_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Types</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBJECT_NODE___GET_TYPES = NODE_OPERATION_COUNT + 1;

	/**
	 * The operation id for the '<em>Get String Value</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBJECT_NODE___GET_STRING_VALUE__STRING = NODE_OPERATION_COUNT + 2;

	/**
	 * The operation id for the '<em>Get Values</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBJECT_NODE___GET_VALUES__STRING = NODE_OPERATION_COUNT + 3;

	/**
	 * The operation id for the '<em>Get URI Values</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBJECT_NODE___GET_URI_VALUES__STRING = NODE_OPERATION_COUNT + 4;

	/**
	 * The operation id for the '<em>Get Object</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBJECT_NODE___GET_OBJECT__STRING = NODE_OPERATION_COUNT + 5;

	/**
	 * The number of operations of the '<em>Subject Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBJECT_NODE_OPERATION_COUNT = NODE_OPERATION_COUNT + 6;

	/**
	 * The meta object id for the '{@link org.gecko.smartmodels.rdf.model.rdf.impl.NamedGraphImpl <em>Named Graph</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.gecko.smartmodels.rdf.model.rdf.impl.NamedGraphImpl
	 * @see org.gecko.smartmodels.rdf.model.rdf.impl.RDFPackageImpl#getNamedGraph()
	 * @generated
	 */
	int NAMED_GRAPH = 7;

	/**
	 * The feature id for the '<em><b>URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_GRAPH__URI = RDF_GRAPH__URI;

	/**
	 * The feature id for the '<em><b>Namespace</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_GRAPH__NAMESPACE = RDF_GRAPH__NAMESPACE;

	/**
	 * The feature id for the '<em><b>Nodes</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_GRAPH__NODES = RDF_GRAPH__NODES;

	/**
	 * The feature id for the '<em><b>Resources</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_GRAPH__RESOURCES = RDF_GRAPH__RESOURCES;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_GRAPH__PROPERTIES = RDF_GRAPH__PROPERTIES;

	/**
	 * The feature id for the '<em><b>Blank Nodes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_GRAPH__BLANK_NODES = RDF_GRAPH__BLANK_NODES;

	/**
	 * The feature id for the '<em><b>Datatypes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_GRAPH__DATATYPES = RDF_GRAPH__DATATYPES;

	/**
	 * The feature id for the '<em><b>Literals</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_GRAPH__LITERALS = RDF_GRAPH__LITERALS;

	/**
	 * The feature id for the '<em><b>Triples</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_GRAPH__TRIPLES = RDF_GRAPH__TRIPLES;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_GRAPH__LABEL = RDF_GRAPH_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_GRAPH__COMMENT = RDF_GRAPH_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Object Of</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_GRAPH__OBJECT_OF = RDF_GRAPH_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Document</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_GRAPH__DOCUMENT = RDF_GRAPH_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Named Graph</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_GRAPH_FEATURE_COUNT = RDF_GRAPH_FEATURE_COUNT + 4;

	/**
	 * The operation id for the '<em>Get Local Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_GRAPH___GET_LOCAL_NAME = RDF_GRAPH___GET_LOCAL_NAME;

	/**
	 * The operation id for the '<em>Is Equivalent To</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_GRAPH___IS_EQUIVALENT_TO__RDFGRAPH = RDF_GRAPH___IS_EQUIVALENT_TO__RDFGRAPH;

	/**
	 * The operation id for the '<em>Add</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_GRAPH___ADD__RDFGRAPH = RDF_GRAPH___ADD__RDFGRAPH;

	/**
	 * The operation id for the '<em>Union</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_GRAPH___UNION__RDFGRAPH = RDF_GRAPH___UNION__RDFGRAPH;

	/**
	 * The operation id for the '<em>Diff</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_GRAPH___DIFF__RDFGRAPH = RDF_GRAPH___DIFF__RDFGRAPH;

	/**
	 * The operation id for the '<em>Add Triple</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_GRAPH___ADD_TRIPLE__NODE_PROPERTY_NODE = RDF_GRAPH___ADD_TRIPLE__NODE_PROPERTY_NODE;

	/**
	 * The operation id for the '<em>List Subjects With Property</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_GRAPH___LIST_SUBJECTS_WITH_PROPERTY__PROPERTY = RDF_GRAPH___LIST_SUBJECTS_WITH_PROPERTY__PROPERTY;

	/**
	 * The operation id for the '<em>List Subjects With Property Value</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_GRAPH___LIST_SUBJECTS_WITH_PROPERTY_VALUE__PROPERTY_NODE = RDF_GRAPH___LIST_SUBJECTS_WITH_PROPERTY_VALUE__PROPERTY_NODE;

	/**
	 * The operation id for the '<em>List Subjects</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_GRAPH___LIST_SUBJECTS = RDF_GRAPH___LIST_SUBJECTS;

	/**
	 * The operation id for the '<em>Get Resource</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_GRAPH___GET_RESOURCE__STRING = RDF_GRAPH___GET_RESOURCE__STRING;

	/**
	 * The operation id for the '<em>Get Property</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_GRAPH___GET_PROPERTY__STRING = RDF_GRAPH___GET_PROPERTY__STRING;

	/**
	 * The operation id for the '<em>Get Datatype</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_GRAPH___GET_DATATYPE__STRING = RDF_GRAPH___GET_DATATYPE__STRING;

	/**
	 * The operation id for the '<em>Get Blank Node</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_GRAPH___GET_BLANK_NODE__STRING = RDF_GRAPH___GET_BLANK_NODE__STRING;

	/**
	 * The operation id for the '<em>List All Triples</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_GRAPH___LIST_ALL_TRIPLES = RDF_GRAPH___LIST_ALL_TRIPLES;

	/**
	 * The operation id for the '<em>List All Resources</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_GRAPH___LIST_ALL_RESOURCES = RDF_GRAPH___LIST_ALL_RESOURCES;

	/**
	 * The operation id for the '<em>List All Properties</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_GRAPH___LIST_ALL_PROPERTIES = RDF_GRAPH___LIST_ALL_PROPERTIES;

	/**
	 * The number of operations of the '<em>Named Graph</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_GRAPH_OPERATION_COUNT = RDF_GRAPH_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.gecko.smartmodels.rdf.model.rdf.impl.BlankNodeImpl <em>Blank Node</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.gecko.smartmodels.rdf.model.rdf.impl.BlankNodeImpl
	 * @see org.gecko.smartmodels.rdf.model.rdf.impl.RDFPackageImpl#getBlankNode()
	 * @generated
	 */
	int BLANK_NODE = 8;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLANK_NODE__LABEL = SUBJECT_NODE__LABEL;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLANK_NODE__COMMENT = SUBJECT_NODE__COMMENT;

	/**
	 * The feature id for the '<em><b>Object Of</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLANK_NODE__OBJECT_OF = SUBJECT_NODE__OBJECT_OF;

	/**
	 * The feature id for the '<em><b>Subject Of</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLANK_NODE__SUBJECT_OF = SUBJECT_NODE__SUBJECT_OF;

	/**
	 * The feature id for the '<em><b>Node ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLANK_NODE__NODE_ID = SUBJECT_NODE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Graph</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLANK_NODE__GRAPH = SUBJECT_NODE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Blank Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLANK_NODE_FEATURE_COUNT = SUBJECT_NODE_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>Is Type Of</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLANK_NODE___IS_TYPE_OF__STRING = SUBJECT_NODE___IS_TYPE_OF__STRING;

	/**
	 * The operation id for the '<em>Get Types</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLANK_NODE___GET_TYPES = SUBJECT_NODE___GET_TYPES;

	/**
	 * The operation id for the '<em>Get String Value</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLANK_NODE___GET_STRING_VALUE__STRING = SUBJECT_NODE___GET_STRING_VALUE__STRING;

	/**
	 * The operation id for the '<em>Get Values</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLANK_NODE___GET_VALUES__STRING = SUBJECT_NODE___GET_VALUES__STRING;

	/**
	 * The operation id for the '<em>Get URI Values</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLANK_NODE___GET_URI_VALUES__STRING = SUBJECT_NODE___GET_URI_VALUES__STRING;

	/**
	 * The operation id for the '<em>Get Object</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLANK_NODE___GET_OBJECT__STRING = SUBJECT_NODE___GET_OBJECT__STRING;

	/**
	 * The number of operations of the '<em>Blank Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLANK_NODE_OPERATION_COUNT = SUBJECT_NODE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.gecko.smartmodels.rdf.model.rdf.impl.ResourceImpl <em>Resource</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.gecko.smartmodels.rdf.model.rdf.impl.ResourceImpl
	 * @see org.gecko.smartmodels.rdf.model.rdf.impl.RDFPackageImpl#getResource()
	 * @generated
	 */
	int RESOURCE = 10;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE__LABEL = SUBJECT_NODE__LABEL;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE__COMMENT = SUBJECT_NODE__COMMENT;

	/**
	 * The feature id for the '<em><b>Object Of</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE__OBJECT_OF = SUBJECT_NODE__OBJECT_OF;

	/**
	 * The feature id for the '<em><b>Subject Of</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE__SUBJECT_OF = SUBJECT_NODE__SUBJECT_OF;

	/**
	 * The feature id for the '<em><b>URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE__URI = SUBJECT_NODE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Namespace</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE__NAMESPACE = SUBJECT_NODE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Graph</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE__GRAPH = SUBJECT_NODE_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Resource</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE_FEATURE_COUNT = SUBJECT_NODE_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>Get Types</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE___GET_TYPES = SUBJECT_NODE___GET_TYPES;

	/**
	 * The operation id for the '<em>Get String Value</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE___GET_STRING_VALUE__STRING = SUBJECT_NODE___GET_STRING_VALUE__STRING;

	/**
	 * The operation id for the '<em>Get Values</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE___GET_VALUES__STRING = SUBJECT_NODE___GET_VALUES__STRING;

	/**
	 * The operation id for the '<em>Get URI Values</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE___GET_URI_VALUES__STRING = SUBJECT_NODE___GET_URI_VALUES__STRING;

	/**
	 * The operation id for the '<em>Get Object</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE___GET_OBJECT__STRING = SUBJECT_NODE___GET_OBJECT__STRING;

	/**
	 * The operation id for the '<em>Get Local Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE___GET_LOCAL_NAME = SUBJECT_NODE_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Is Type Of</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE___IS_TYPE_OF__STRING = SUBJECT_NODE_OPERATION_COUNT + 1;

	/**
	 * The number of operations of the '<em>Resource</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE_OPERATION_COUNT = SUBJECT_NODE_OPERATION_COUNT + 2;

	/**
	 * The meta object id for the '{@link org.gecko.smartmodels.rdf.model.rdf.impl.TripleNodeImpl <em>Triple Node</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.gecko.smartmodels.rdf.model.rdf.impl.TripleNodeImpl
	 * @see org.gecko.smartmodels.rdf.model.rdf.impl.RDFPackageImpl#getTripleNode()
	 * @generated
	 */
	int TRIPLE_NODE = 9;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIPLE_NODE__LABEL = RESOURCE__LABEL;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIPLE_NODE__COMMENT = RESOURCE__COMMENT;

	/**
	 * The feature id for the '<em><b>Object Of</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIPLE_NODE__OBJECT_OF = RESOURCE__OBJECT_OF;

	/**
	 * The feature id for the '<em><b>Subject Of</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIPLE_NODE__SUBJECT_OF = RESOURCE__SUBJECT_OF;

	/**
	 * The feature id for the '<em><b>URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIPLE_NODE__URI = RESOURCE__URI;

	/**
	 * The feature id for the '<em><b>Namespace</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIPLE_NODE__NAMESPACE = RESOURCE__NAMESPACE;

	/**
	 * The feature id for the '<em><b>Graph</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIPLE_NODE__GRAPH = RESOURCE__GRAPH;

	/**
	 * The feature id for the '<em><b>Triple</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIPLE_NODE__TRIPLE = RESOURCE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Triple Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIPLE_NODE_FEATURE_COUNT = RESOURCE_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Get Types</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIPLE_NODE___GET_TYPES = RESOURCE___GET_TYPES;

	/**
	 * The operation id for the '<em>Get String Value</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIPLE_NODE___GET_STRING_VALUE__STRING = RESOURCE___GET_STRING_VALUE__STRING;

	/**
	 * The operation id for the '<em>Get Values</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIPLE_NODE___GET_VALUES__STRING = RESOURCE___GET_VALUES__STRING;

	/**
	 * The operation id for the '<em>Get URI Values</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIPLE_NODE___GET_URI_VALUES__STRING = RESOURCE___GET_URI_VALUES__STRING;

	/**
	 * The operation id for the '<em>Get Object</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIPLE_NODE___GET_OBJECT__STRING = RESOURCE___GET_OBJECT__STRING;

	/**
	 * The operation id for the '<em>Get Local Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIPLE_NODE___GET_LOCAL_NAME = RESOURCE___GET_LOCAL_NAME;

	/**
	 * The operation id for the '<em>Is Type Of</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIPLE_NODE___IS_TYPE_OF__STRING = RESOURCE___IS_TYPE_OF__STRING;

	/**
	 * The number of operations of the '<em>Triple Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIPLE_NODE_OPERATION_COUNT = RESOURCE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.gecko.smartmodels.rdf.model.rdf.impl.PropertyImpl <em>Property</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.gecko.smartmodels.rdf.model.rdf.impl.PropertyImpl
	 * @see org.gecko.smartmodels.rdf.model.rdf.impl.RDFPackageImpl#getProperty()
	 * @generated
	 */
	int PROPERTY = 11;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY__LABEL = RESOURCE__LABEL;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY__COMMENT = RESOURCE__COMMENT;

	/**
	 * The feature id for the '<em><b>Object Of</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY__OBJECT_OF = RESOURCE__OBJECT_OF;

	/**
	 * The feature id for the '<em><b>Subject Of</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY__SUBJECT_OF = RESOURCE__SUBJECT_OF;

	/**
	 * The feature id for the '<em><b>URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY__URI = RESOURCE__URI;

	/**
	 * The feature id for the '<em><b>Namespace</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY__NAMESPACE = RESOURCE__NAMESPACE;

	/**
	 * The feature id for the '<em><b>Graph</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY__GRAPH = RESOURCE__GRAPH;

	/**
	 * The feature id for the '<em><b>Predicate Of</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY__PREDICATE_OF = RESOURCE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY_FEATURE_COUNT = RESOURCE_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Get Types</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY___GET_TYPES = RESOURCE___GET_TYPES;

	/**
	 * The operation id for the '<em>Get String Value</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY___GET_STRING_VALUE__STRING = RESOURCE___GET_STRING_VALUE__STRING;

	/**
	 * The operation id for the '<em>Get Values</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY___GET_VALUES__STRING = RESOURCE___GET_VALUES__STRING;

	/**
	 * The operation id for the '<em>Get URI Values</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY___GET_URI_VALUES__STRING = RESOURCE___GET_URI_VALUES__STRING;

	/**
	 * The operation id for the '<em>Get Object</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY___GET_OBJECT__STRING = RESOURCE___GET_OBJECT__STRING;

	/**
	 * The operation id for the '<em>Get Local Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY___GET_LOCAL_NAME = RESOURCE___GET_LOCAL_NAME;

	/**
	 * The operation id for the '<em>Is Type Of</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY___IS_TYPE_OF__STRING = RESOURCE___IS_TYPE_OF__STRING;

	/**
	 * The number of operations of the '<em>Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY_OPERATION_COUNT = RESOURCE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.gecko.smartmodels.rdf.model.rdf.impl.DatatypeImpl <em>Datatype</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.gecko.smartmodels.rdf.model.rdf.impl.DatatypeImpl
	 * @see org.gecko.smartmodels.rdf.model.rdf.impl.RDFPackageImpl#getDatatype()
	 * @generated
	 */
	int DATATYPE = 12;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATATYPE__LABEL = RESOURCE__LABEL;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATATYPE__COMMENT = RESOURCE__COMMENT;

	/**
	 * The feature id for the '<em><b>Object Of</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATATYPE__OBJECT_OF = RESOURCE__OBJECT_OF;

	/**
	 * The feature id for the '<em><b>Subject Of</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATATYPE__SUBJECT_OF = RESOURCE__SUBJECT_OF;

	/**
	 * The feature id for the '<em><b>URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATATYPE__URI = RESOURCE__URI;

	/**
	 * The feature id for the '<em><b>Namespace</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATATYPE__NAMESPACE = RESOURCE__NAMESPACE;

	/**
	 * The feature id for the '<em><b>Graph</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATATYPE__GRAPH = RESOURCE__GRAPH;

	/**
	 * The number of structural features of the '<em>Datatype</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATATYPE_FEATURE_COUNT = RESOURCE_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Types</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATATYPE___GET_TYPES = RESOURCE___GET_TYPES;

	/**
	 * The operation id for the '<em>Get String Value</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATATYPE___GET_STRING_VALUE__STRING = RESOURCE___GET_STRING_VALUE__STRING;

	/**
	 * The operation id for the '<em>Get Values</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATATYPE___GET_VALUES__STRING = RESOURCE___GET_VALUES__STRING;

	/**
	 * The operation id for the '<em>Get URI Values</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATATYPE___GET_URI_VALUES__STRING = RESOURCE___GET_URI_VALUES__STRING;

	/**
	 * The operation id for the '<em>Get Object</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATATYPE___GET_OBJECT__STRING = RESOURCE___GET_OBJECT__STRING;

	/**
	 * The operation id for the '<em>Get Local Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATATYPE___GET_LOCAL_NAME = RESOURCE___GET_LOCAL_NAME;

	/**
	 * The operation id for the '<em>Is Type Of</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATATYPE___IS_TYPE_OF__STRING = RESOURCE___IS_TYPE_OF__STRING;

	/**
	 * The number of operations of the '<em>Datatype</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATATYPE_OPERATION_COUNT = RESOURCE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.gecko.smartmodels.rdf.model.rdf.impl.LiteralImpl <em>Literal</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.gecko.smartmodels.rdf.model.rdf.impl.LiteralImpl
	 * @see org.gecko.smartmodels.rdf.model.rdf.impl.RDFPackageImpl#getLiteral()
	 * @generated
	 */
	int LITERAL = 13;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LITERAL__LABEL = NODE__LABEL;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LITERAL__COMMENT = NODE__COMMENT;

	/**
	 * The feature id for the '<em><b>Object Of</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LITERAL__OBJECT_OF = NODE__OBJECT_OF;

	/**
	 * The feature id for the '<em><b>Lexical Form</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LITERAL__LEXICAL_FORM = NODE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Lang</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LITERAL__LANG = NODE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Datatype</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LITERAL__DATATYPE = NODE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Graph</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LITERAL__GRAPH = NODE_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Literal</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LITERAL_FEATURE_COUNT = NODE_FEATURE_COUNT + 4;

	/**
	 * The number of operations of the '<em>Literal</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LITERAL_OPERATION_COUNT = NODE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.gecko.smartmodels.rdf.model.rdf.impl.RDFSContainerImpl <em>SContainer</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.gecko.smartmodels.rdf.model.rdf.impl.RDFSContainerImpl
	 * @see org.gecko.smartmodels.rdf.model.rdf.impl.RDFPackageImpl#getRDFSContainer()
	 * @generated
	 */
	int RDF_SCONTAINER = 14;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RDF_SCONTAINER__LABEL = BLANK_NODE__LABEL;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RDF_SCONTAINER__COMMENT = BLANK_NODE__COMMENT;

	/**
	 * The feature id for the '<em><b>Object Of</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RDF_SCONTAINER__OBJECT_OF = BLANK_NODE__OBJECT_OF;

	/**
	 * The feature id for the '<em><b>Subject Of</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RDF_SCONTAINER__SUBJECT_OF = BLANK_NODE__SUBJECT_OF;

	/**
	 * The feature id for the '<em><b>Node ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RDF_SCONTAINER__NODE_ID = BLANK_NODE__NODE_ID;

	/**
	 * The feature id for the '<em><b>Graph</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RDF_SCONTAINER__GRAPH = BLANK_NODE__GRAPH;

	/**
	 * The number of structural features of the '<em>SContainer</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RDF_SCONTAINER_FEATURE_COUNT = BLANK_NODE_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Is Type Of</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RDF_SCONTAINER___IS_TYPE_OF__STRING = BLANK_NODE___IS_TYPE_OF__STRING;

	/**
	 * The operation id for the '<em>Get Types</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RDF_SCONTAINER___GET_TYPES = BLANK_NODE___GET_TYPES;

	/**
	 * The operation id for the '<em>Get String Value</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RDF_SCONTAINER___GET_STRING_VALUE__STRING = BLANK_NODE___GET_STRING_VALUE__STRING;

	/**
	 * The operation id for the '<em>Get Values</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RDF_SCONTAINER___GET_VALUES__STRING = BLANK_NODE___GET_VALUES__STRING;

	/**
	 * The operation id for the '<em>Get URI Values</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RDF_SCONTAINER___GET_URI_VALUES__STRING = BLANK_NODE___GET_URI_VALUES__STRING;

	/**
	 * The operation id for the '<em>Get Object</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RDF_SCONTAINER___GET_OBJECT__STRING = BLANK_NODE___GET_OBJECT__STRING;

	/**
	 * The number of operations of the '<em>SContainer</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RDF_SCONTAINER_OPERATION_COUNT = BLANK_NODE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.gecko.smartmodels.rdf.model.rdf.impl.RDFBagImpl <em>Bag</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.gecko.smartmodels.rdf.model.rdf.impl.RDFBagImpl
	 * @see org.gecko.smartmodels.rdf.model.rdf.impl.RDFPackageImpl#getRDFBag()
	 * @generated
	 */
	int RDF_BAG = 15;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RDF_BAG__LABEL = RDF_SCONTAINER__LABEL;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RDF_BAG__COMMENT = RDF_SCONTAINER__COMMENT;

	/**
	 * The feature id for the '<em><b>Object Of</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RDF_BAG__OBJECT_OF = RDF_SCONTAINER__OBJECT_OF;

	/**
	 * The feature id for the '<em><b>Subject Of</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RDF_BAG__SUBJECT_OF = RDF_SCONTAINER__SUBJECT_OF;

	/**
	 * The feature id for the '<em><b>Node ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RDF_BAG__NODE_ID = RDF_SCONTAINER__NODE_ID;

	/**
	 * The feature id for the '<em><b>Graph</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RDF_BAG__GRAPH = RDF_SCONTAINER__GRAPH;

	/**
	 * The feature id for the '<em><b>Elements</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RDF_BAG__ELEMENTS = RDF_SCONTAINER_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Bag</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RDF_BAG_FEATURE_COUNT = RDF_SCONTAINER_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Is Type Of</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RDF_BAG___IS_TYPE_OF__STRING = RDF_SCONTAINER___IS_TYPE_OF__STRING;

	/**
	 * The operation id for the '<em>Get Types</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RDF_BAG___GET_TYPES = RDF_SCONTAINER___GET_TYPES;

	/**
	 * The operation id for the '<em>Get String Value</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RDF_BAG___GET_STRING_VALUE__STRING = RDF_SCONTAINER___GET_STRING_VALUE__STRING;

	/**
	 * The operation id for the '<em>Get Values</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RDF_BAG___GET_VALUES__STRING = RDF_SCONTAINER___GET_VALUES__STRING;

	/**
	 * The operation id for the '<em>Get URI Values</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RDF_BAG___GET_URI_VALUES__STRING = RDF_SCONTAINER___GET_URI_VALUES__STRING;

	/**
	 * The operation id for the '<em>Get Object</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RDF_BAG___GET_OBJECT__STRING = RDF_SCONTAINER___GET_OBJECT__STRING;

	/**
	 * The number of operations of the '<em>Bag</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RDF_BAG_OPERATION_COUNT = RDF_SCONTAINER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.gecko.smartmodels.rdf.model.rdf.impl.RDFAltImpl <em>Alt</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.gecko.smartmodels.rdf.model.rdf.impl.RDFAltImpl
	 * @see org.gecko.smartmodels.rdf.model.rdf.impl.RDFPackageImpl#getRDFAlt()
	 * @generated
	 */
	int RDF_ALT = 16;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RDF_ALT__LABEL = RDF_SCONTAINER__LABEL;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RDF_ALT__COMMENT = RDF_SCONTAINER__COMMENT;

	/**
	 * The feature id for the '<em><b>Object Of</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RDF_ALT__OBJECT_OF = RDF_SCONTAINER__OBJECT_OF;

	/**
	 * The feature id for the '<em><b>Subject Of</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RDF_ALT__SUBJECT_OF = RDF_SCONTAINER__SUBJECT_OF;

	/**
	 * The feature id for the '<em><b>Node ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RDF_ALT__NODE_ID = RDF_SCONTAINER__NODE_ID;

	/**
	 * The feature id for the '<em><b>Graph</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RDF_ALT__GRAPH = RDF_SCONTAINER__GRAPH;

	/**
	 * The feature id for the '<em><b>Elements</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RDF_ALT__ELEMENTS = RDF_SCONTAINER_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Alt</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RDF_ALT_FEATURE_COUNT = RDF_SCONTAINER_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Is Type Of</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RDF_ALT___IS_TYPE_OF__STRING = RDF_SCONTAINER___IS_TYPE_OF__STRING;

	/**
	 * The operation id for the '<em>Get Types</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RDF_ALT___GET_TYPES = RDF_SCONTAINER___GET_TYPES;

	/**
	 * The operation id for the '<em>Get String Value</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RDF_ALT___GET_STRING_VALUE__STRING = RDF_SCONTAINER___GET_STRING_VALUE__STRING;

	/**
	 * The operation id for the '<em>Get Values</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RDF_ALT___GET_VALUES__STRING = RDF_SCONTAINER___GET_VALUES__STRING;

	/**
	 * The operation id for the '<em>Get URI Values</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RDF_ALT___GET_URI_VALUES__STRING = RDF_SCONTAINER___GET_URI_VALUES__STRING;

	/**
	 * The operation id for the '<em>Get Object</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RDF_ALT___GET_OBJECT__STRING = RDF_SCONTAINER___GET_OBJECT__STRING;

	/**
	 * The number of operations of the '<em>Alt</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RDF_ALT_OPERATION_COUNT = RDF_SCONTAINER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.gecko.smartmodels.rdf.model.rdf.impl.RDFSeqImpl <em>Seq</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.gecko.smartmodels.rdf.model.rdf.impl.RDFSeqImpl
	 * @see org.gecko.smartmodels.rdf.model.rdf.impl.RDFPackageImpl#getRDFSeq()
	 * @generated
	 */
	int RDF_SEQ = 17;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RDF_SEQ__LABEL = RDF_SCONTAINER__LABEL;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RDF_SEQ__COMMENT = RDF_SCONTAINER__COMMENT;

	/**
	 * The feature id for the '<em><b>Object Of</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RDF_SEQ__OBJECT_OF = RDF_SCONTAINER__OBJECT_OF;

	/**
	 * The feature id for the '<em><b>Subject Of</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RDF_SEQ__SUBJECT_OF = RDF_SCONTAINER__SUBJECT_OF;

	/**
	 * The feature id for the '<em><b>Node ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RDF_SEQ__NODE_ID = RDF_SCONTAINER__NODE_ID;

	/**
	 * The feature id for the '<em><b>Graph</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RDF_SEQ__GRAPH = RDF_SCONTAINER__GRAPH;

	/**
	 * The feature id for the '<em><b>Elements</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RDF_SEQ__ELEMENTS = RDF_SCONTAINER_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Seq</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RDF_SEQ_FEATURE_COUNT = RDF_SCONTAINER_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Is Type Of</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RDF_SEQ___IS_TYPE_OF__STRING = RDF_SCONTAINER___IS_TYPE_OF__STRING;

	/**
	 * The operation id for the '<em>Get Types</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RDF_SEQ___GET_TYPES = RDF_SCONTAINER___GET_TYPES;

	/**
	 * The operation id for the '<em>Get String Value</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RDF_SEQ___GET_STRING_VALUE__STRING = RDF_SCONTAINER___GET_STRING_VALUE__STRING;

	/**
	 * The operation id for the '<em>Get Values</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RDF_SEQ___GET_VALUES__STRING = RDF_SCONTAINER___GET_VALUES__STRING;

	/**
	 * The operation id for the '<em>Get URI Values</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RDF_SEQ___GET_URI_VALUES__STRING = RDF_SCONTAINER___GET_URI_VALUES__STRING;

	/**
	 * The operation id for the '<em>Get Object</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RDF_SEQ___GET_OBJECT__STRING = RDF_SCONTAINER___GET_OBJECT__STRING;

	/**
	 * The number of operations of the '<em>Seq</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RDF_SEQ_OPERATION_COUNT = RDF_SCONTAINER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.gecko.smartmodels.rdf.model.rdf.impl.RDFListImpl <em>List</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.gecko.smartmodels.rdf.model.rdf.impl.RDFListImpl
	 * @see org.gecko.smartmodels.rdf.model.rdf.impl.RDFPackageImpl#getRDFList()
	 * @generated
	 */
	int RDF_LIST = 18;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RDF_LIST__LABEL = BLANK_NODE__LABEL;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RDF_LIST__COMMENT = BLANK_NODE__COMMENT;

	/**
	 * The feature id for the '<em><b>Object Of</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RDF_LIST__OBJECT_OF = BLANK_NODE__OBJECT_OF;

	/**
	 * The feature id for the '<em><b>Subject Of</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RDF_LIST__SUBJECT_OF = BLANK_NODE__SUBJECT_OF;

	/**
	 * The feature id for the '<em><b>Node ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RDF_LIST__NODE_ID = BLANK_NODE__NODE_ID;

	/**
	 * The feature id for the '<em><b>Graph</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RDF_LIST__GRAPH = BLANK_NODE__GRAPH;

	/**
	 * The feature id for the '<em><b>Elements</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RDF_LIST__ELEMENTS = BLANK_NODE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>List</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RDF_LIST_FEATURE_COUNT = BLANK_NODE_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Is Type Of</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RDF_LIST___IS_TYPE_OF__STRING = BLANK_NODE___IS_TYPE_OF__STRING;

	/**
	 * The operation id for the '<em>Get Types</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RDF_LIST___GET_TYPES = BLANK_NODE___GET_TYPES;

	/**
	 * The operation id for the '<em>Get String Value</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RDF_LIST___GET_STRING_VALUE__STRING = BLANK_NODE___GET_STRING_VALUE__STRING;

	/**
	 * The operation id for the '<em>Get Values</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RDF_LIST___GET_VALUES__STRING = BLANK_NODE___GET_VALUES__STRING;

	/**
	 * The operation id for the '<em>Get URI Values</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RDF_LIST___GET_URI_VALUES__STRING = BLANK_NODE___GET_URI_VALUES__STRING;

	/**
	 * The operation id for the '<em>Get Object</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RDF_LIST___GET_OBJECT__STRING = BLANK_NODE___GET_OBJECT__STRING;

	/**
	 * The number of operations of the '<em>List</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RDF_LIST_OPERATION_COUNT = BLANK_NODE_OPERATION_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link org.gecko.smartmodels.rdf.model.rdf.URIElement <em>URI Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>URI Element</em>'.
	 * @see org.gecko.smartmodels.rdf.model.rdf.URIElement
	 * @generated
	 */
	EClass getURIElement();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.smartmodels.rdf.model.rdf.URIElement#getURI <em>URI</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>URI</em>'.
	 * @see org.gecko.smartmodels.rdf.model.rdf.URIElement#getURI()
	 * @see #getURIElement()
	 * @generated
	 */
	EAttribute getURIElement_URI();

	/**
	 * Returns the meta object for the reference '{@link org.gecko.smartmodels.rdf.model.rdf.URIElement#getNamespace <em>Namespace</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Namespace</em>'.
	 * @see org.gecko.smartmodels.rdf.model.rdf.URIElement#getNamespace()
	 * @see #getURIElement()
	 * @generated
	 */
	EReference getURIElement_Namespace();

	/**
	 * Returns the meta object for the '{@link org.gecko.smartmodels.rdf.model.rdf.URIElement#getLocalName() <em>Get Local Name</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Local Name</em>' operation.
	 * @see org.gecko.smartmodels.rdf.model.rdf.URIElement#getLocalName()
	 * @generated
	 */
	EOperation getURIElement__GetLocalName();

	/**
	 * Returns the meta object for class '{@link org.gecko.smartmodels.rdf.model.rdf.Namespace <em>Namespace</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Namespace</em>'.
	 * @see org.gecko.smartmodels.rdf.model.rdf.Namespace
	 * @generated
	 */
	EClass getNamespace();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.smartmodels.rdf.model.rdf.Namespace#getPrefix <em>Prefix</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Prefix</em>'.
	 * @see org.gecko.smartmodels.rdf.model.rdf.Namespace#getPrefix()
	 * @see #getNamespace()
	 * @generated
	 */
	EAttribute getNamespace_Prefix();

	/**
	 * Returns the meta object for the container reference '{@link org.gecko.smartmodels.rdf.model.rdf.Namespace#getGraph <em>Graph</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Graph</em>'.
	 * @see org.gecko.smartmodels.rdf.model.rdf.Namespace#getGraph()
	 * @see #getNamespace()
	 * @generated
	 */
	EReference getNamespace_Graph();

	/**
	 * Returns the meta object for class '{@link org.gecko.smartmodels.rdf.model.rdf.RDFGraph <em>Graph</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Graph</em>'.
	 * @see org.gecko.smartmodels.rdf.model.rdf.RDFGraph
	 * @generated
	 */
	EClass getRDFGraph();

	/**
	 * Returns the meta object for the attribute list '{@link org.gecko.smartmodels.rdf.model.rdf.RDFGraph#getNodes <em>Nodes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Nodes</em>'.
	 * @see org.gecko.smartmodels.rdf.model.rdf.RDFGraph#getNodes()
	 * @see #getRDFGraph()
	 * @generated
	 */
	EAttribute getRDFGraph_Nodes();

	/**
	 * Returns the meta object for the containment reference list '{@link org.gecko.smartmodels.rdf.model.rdf.RDFGraph#getResources <em>Resources</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Resources</em>'.
	 * @see org.gecko.smartmodels.rdf.model.rdf.RDFGraph#getResources()
	 * @see #getRDFGraph()
	 * @generated
	 */
	EReference getRDFGraph_Resources();

	/**
	 * Returns the meta object for the containment reference list '{@link org.gecko.smartmodels.rdf.model.rdf.RDFGraph#getProperties <em>Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Properties</em>'.
	 * @see org.gecko.smartmodels.rdf.model.rdf.RDFGraph#getProperties()
	 * @see #getRDFGraph()
	 * @generated
	 */
	EReference getRDFGraph_Properties();

	/**
	 * Returns the meta object for the containment reference list '{@link org.gecko.smartmodels.rdf.model.rdf.RDFGraph#getBlankNodes <em>Blank Nodes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Blank Nodes</em>'.
	 * @see org.gecko.smartmodels.rdf.model.rdf.RDFGraph#getBlankNodes()
	 * @see #getRDFGraph()
	 * @generated
	 */
	EReference getRDFGraph_BlankNodes();

	/**
	 * Returns the meta object for the containment reference list '{@link org.gecko.smartmodels.rdf.model.rdf.RDFGraph#getDatatypes <em>Datatypes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Datatypes</em>'.
	 * @see org.gecko.smartmodels.rdf.model.rdf.RDFGraph#getDatatypes()
	 * @see #getRDFGraph()
	 * @generated
	 */
	EReference getRDFGraph_Datatypes();

	/**
	 * Returns the meta object for the containment reference list '{@link org.gecko.smartmodels.rdf.model.rdf.RDFGraph#getLiterals <em>Literals</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Literals</em>'.
	 * @see org.gecko.smartmodels.rdf.model.rdf.RDFGraph#getLiterals()
	 * @see #getRDFGraph()
	 * @generated
	 */
	EReference getRDFGraph_Literals();

	/**
	 * Returns the meta object for the containment reference list '{@link org.gecko.smartmodels.rdf.model.rdf.RDFGraph#getTriples <em>Triples</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Triples</em>'.
	 * @see org.gecko.smartmodels.rdf.model.rdf.RDFGraph#getTriples()
	 * @see #getRDFGraph()
	 * @generated
	 */
	EReference getRDFGraph_Triples();

	/**
	 * Returns the meta object for the '{@link org.gecko.smartmodels.rdf.model.rdf.RDFGraph#isEquivalentTo(org.gecko.smartmodels.rdf.model.rdf.RDFGraph) <em>Is Equivalent To</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Equivalent To</em>' operation.
	 * @see org.gecko.smartmodels.rdf.model.rdf.RDFGraph#isEquivalentTo(org.gecko.smartmodels.rdf.model.rdf.RDFGraph)
	 * @generated
	 */
	EOperation getRDFGraph__IsEquivalentTo__RDFGraph();

	/**
	 * Returns the meta object for the '{@link org.gecko.smartmodels.rdf.model.rdf.RDFGraph#add(org.gecko.smartmodels.rdf.model.rdf.RDFGraph) <em>Add</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add</em>' operation.
	 * @see org.gecko.smartmodels.rdf.model.rdf.RDFGraph#add(org.gecko.smartmodels.rdf.model.rdf.RDFGraph)
	 * @generated
	 */
	EOperation getRDFGraph__Add__RDFGraph();

	/**
	 * Returns the meta object for the '{@link org.gecko.smartmodels.rdf.model.rdf.RDFGraph#union(org.gecko.smartmodels.rdf.model.rdf.RDFGraph) <em>Union</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Union</em>' operation.
	 * @see org.gecko.smartmodels.rdf.model.rdf.RDFGraph#union(org.gecko.smartmodels.rdf.model.rdf.RDFGraph)
	 * @generated
	 */
	EOperation getRDFGraph__Union__RDFGraph();

	/**
	 * Returns the meta object for the '{@link org.gecko.smartmodels.rdf.model.rdf.RDFGraph#diff(org.gecko.smartmodels.rdf.model.rdf.RDFGraph) <em>Diff</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Diff</em>' operation.
	 * @see org.gecko.smartmodels.rdf.model.rdf.RDFGraph#diff(org.gecko.smartmodels.rdf.model.rdf.RDFGraph)
	 * @generated
	 */
	EOperation getRDFGraph__Diff__RDFGraph();

	/**
	 * Returns the meta object for the '{@link org.gecko.smartmodels.rdf.model.rdf.RDFGraph#addTriple(org.gecko.smartmodels.rdf.model.rdf.Node, org.gecko.smartmodels.rdf.model.rdf.Property, org.gecko.smartmodels.rdf.model.rdf.Node) <em>Add Triple</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Triple</em>' operation.
	 * @see org.gecko.smartmodels.rdf.model.rdf.RDFGraph#addTriple(org.gecko.smartmodels.rdf.model.rdf.Node, org.gecko.smartmodels.rdf.model.rdf.Property, org.gecko.smartmodels.rdf.model.rdf.Node)
	 * @generated
	 */
	EOperation getRDFGraph__AddTriple__Node_Property_Node();

	/**
	 * Returns the meta object for the '{@link org.gecko.smartmodels.rdf.model.rdf.RDFGraph#listSubjectsWithProperty(org.gecko.smartmodels.rdf.model.rdf.Property) <em>List Subjects With Property</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>List Subjects With Property</em>' operation.
	 * @see org.gecko.smartmodels.rdf.model.rdf.RDFGraph#listSubjectsWithProperty(org.gecko.smartmodels.rdf.model.rdf.Property)
	 * @generated
	 */
	EOperation getRDFGraph__ListSubjectsWithProperty__Property();

	/**
	 * Returns the meta object for the '{@link org.gecko.smartmodels.rdf.model.rdf.RDFGraph#listSubjectsWithPropertyValue(org.gecko.smartmodels.rdf.model.rdf.Property, org.gecko.smartmodels.rdf.model.rdf.Node) <em>List Subjects With Property Value</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>List Subjects With Property Value</em>' operation.
	 * @see org.gecko.smartmodels.rdf.model.rdf.RDFGraph#listSubjectsWithPropertyValue(org.gecko.smartmodels.rdf.model.rdf.Property, org.gecko.smartmodels.rdf.model.rdf.Node)
	 * @generated
	 */
	EOperation getRDFGraph__ListSubjectsWithPropertyValue__Property_Node();

	/**
	 * Returns the meta object for the '{@link org.gecko.smartmodels.rdf.model.rdf.RDFGraph#listSubjects() <em>List Subjects</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>List Subjects</em>' operation.
	 * @see org.gecko.smartmodels.rdf.model.rdf.RDFGraph#listSubjects()
	 * @generated
	 */
	EOperation getRDFGraph__ListSubjects();

	/**
	 * Returns the meta object for the '{@link org.gecko.smartmodels.rdf.model.rdf.RDFGraph#getResource(java.lang.String) <em>Get Resource</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Resource</em>' operation.
	 * @see org.gecko.smartmodels.rdf.model.rdf.RDFGraph#getResource(java.lang.String)
	 * @generated
	 */
	EOperation getRDFGraph__GetResource__String();

	/**
	 * Returns the meta object for the '{@link org.gecko.smartmodels.rdf.model.rdf.RDFGraph#getProperty(java.lang.String) <em>Get Property</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Property</em>' operation.
	 * @see org.gecko.smartmodels.rdf.model.rdf.RDFGraph#getProperty(java.lang.String)
	 * @generated
	 */
	EOperation getRDFGraph__GetProperty__String();

	/**
	 * Returns the meta object for the '{@link org.gecko.smartmodels.rdf.model.rdf.RDFGraph#getDatatype(java.lang.String) <em>Get Datatype</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Datatype</em>' operation.
	 * @see org.gecko.smartmodels.rdf.model.rdf.RDFGraph#getDatatype(java.lang.String)
	 * @generated
	 */
	EOperation getRDFGraph__GetDatatype__String();

	/**
	 * Returns the meta object for the '{@link org.gecko.smartmodels.rdf.model.rdf.RDFGraph#getBlankNode(java.lang.String) <em>Get Blank Node</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Blank Node</em>' operation.
	 * @see org.gecko.smartmodels.rdf.model.rdf.RDFGraph#getBlankNode(java.lang.String)
	 * @generated
	 */
	EOperation getRDFGraph__GetBlankNode__String();

	/**
	 * Returns the meta object for the '{@link org.gecko.smartmodels.rdf.model.rdf.RDFGraph#listAllTriples() <em>List All Triples</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>List All Triples</em>' operation.
	 * @see org.gecko.smartmodels.rdf.model.rdf.RDFGraph#listAllTriples()
	 * @generated
	 */
	EOperation getRDFGraph__ListAllTriples();

	/**
	 * Returns the meta object for the '{@link org.gecko.smartmodels.rdf.model.rdf.RDFGraph#listAllResources() <em>List All Resources</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>List All Resources</em>' operation.
	 * @see org.gecko.smartmodels.rdf.model.rdf.RDFGraph#listAllResources()
	 * @generated
	 */
	EOperation getRDFGraph__ListAllResources();

	/**
	 * Returns the meta object for the '{@link org.gecko.smartmodels.rdf.model.rdf.RDFGraph#listAllProperties() <em>List All Properties</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>List All Properties</em>' operation.
	 * @see org.gecko.smartmodels.rdf.model.rdf.RDFGraph#listAllProperties()
	 * @generated
	 */
	EOperation getRDFGraph__ListAllProperties();

	/**
	 * Returns the meta object for class '{@link org.gecko.smartmodels.rdf.model.rdf.DocumentGraph <em>Document Graph</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Document Graph</em>'.
	 * @see org.gecko.smartmodels.rdf.model.rdf.DocumentGraph
	 * @generated
	 */
	EClass getDocumentGraph();

	/**
	 * Returns the meta object for the containment reference list '{@link org.gecko.smartmodels.rdf.model.rdf.DocumentGraph#getNamespaces <em>Namespaces</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Namespaces</em>'.
	 * @see org.gecko.smartmodels.rdf.model.rdf.DocumentGraph#getNamespaces()
	 * @see #getDocumentGraph()
	 * @generated
	 */
	EReference getDocumentGraph_Namespaces();

	/**
	 * Returns the meta object for the reference list '{@link org.gecko.smartmodels.rdf.model.rdf.DocumentGraph#getSubGraphs <em>Sub Graphs</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Sub Graphs</em>'.
	 * @see org.gecko.smartmodels.rdf.model.rdf.DocumentGraph#getSubGraphs()
	 * @see #getDocumentGraph()
	 * @generated
	 */
	EReference getDocumentGraph_SubGraphs();

	/**
	 * Returns the meta object for the '{@link org.gecko.smartmodels.rdf.model.rdf.DocumentGraph#getNamedGraph(java.lang.String) <em>Get Named Graph</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Named Graph</em>' operation.
	 * @see org.gecko.smartmodels.rdf.model.rdf.DocumentGraph#getNamedGraph(java.lang.String)
	 * @generated
	 */
	EOperation getDocumentGraph__GetNamedGraph__String();

	/**
	 * Returns the meta object for class '{@link org.gecko.smartmodels.rdf.model.rdf.Triple <em>Triple</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Triple</em>'.
	 * @see org.gecko.smartmodels.rdf.model.rdf.Triple
	 * @generated
	 */
	EClass getTriple();

	/**
	 * Returns the meta object for the reference '{@link org.gecko.smartmodels.rdf.model.rdf.Triple#getSubject <em>Subject</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Subject</em>'.
	 * @see org.gecko.smartmodels.rdf.model.rdf.Triple#getSubject()
	 * @see #getTriple()
	 * @generated
	 */
	EReference getTriple_Subject();

	/**
	 * Returns the meta object for the reference '{@link org.gecko.smartmodels.rdf.model.rdf.Triple#getPredicate <em>Predicate</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Predicate</em>'.
	 * @see org.gecko.smartmodels.rdf.model.rdf.Triple#getPredicate()
	 * @see #getTriple()
	 * @generated
	 */
	EReference getTriple_Predicate();

	/**
	 * Returns the meta object for the reference '{@link org.gecko.smartmodels.rdf.model.rdf.Triple#getObject <em>Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Object</em>'.
	 * @see org.gecko.smartmodels.rdf.model.rdf.Triple#getObject()
	 * @see #getTriple()
	 * @generated
	 */
	EReference getTriple_Object();

	/**
	 * Returns the meta object for the container reference '{@link org.gecko.smartmodels.rdf.model.rdf.Triple#getGraph <em>Graph</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Graph</em>'.
	 * @see org.gecko.smartmodels.rdf.model.rdf.Triple#getGraph()
	 * @see #getTriple()
	 * @generated
	 */
	EReference getTriple_Graph();

	/**
	 * Returns the meta object for class '{@link org.gecko.smartmodels.rdf.model.rdf.Node <em>Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Node</em>'.
	 * @see org.gecko.smartmodels.rdf.model.rdf.Node
	 * @generated
	 */
	EClass getNode();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.smartmodels.rdf.model.rdf.Node#getLabel <em>Label</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Label</em>'.
	 * @see org.gecko.smartmodels.rdf.model.rdf.Node#getLabel()
	 * @see #getNode()
	 * @generated
	 */
	EAttribute getNode_Label();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.smartmodels.rdf.model.rdf.Node#getComment <em>Comment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Comment</em>'.
	 * @see org.gecko.smartmodels.rdf.model.rdf.Node#getComment()
	 * @see #getNode()
	 * @generated
	 */
	EAttribute getNode_Comment();

	/**
	 * Returns the meta object for the reference list '{@link org.gecko.smartmodels.rdf.model.rdf.Node#getObjectOf <em>Object Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Object Of</em>'.
	 * @see org.gecko.smartmodels.rdf.model.rdf.Node#getObjectOf()
	 * @see #getNode()
	 * @generated
	 */
	EReference getNode_ObjectOf();

	/**
	 * Returns the meta object for class '{@link org.gecko.smartmodels.rdf.model.rdf.SubjectNode <em>Subject Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Subject Node</em>'.
	 * @see org.gecko.smartmodels.rdf.model.rdf.SubjectNode
	 * @generated
	 */
	EClass getSubjectNode();

	/**
	 * Returns the meta object for the reference list '{@link org.gecko.smartmodels.rdf.model.rdf.SubjectNode#getSubjectOf <em>Subject Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Subject Of</em>'.
	 * @see org.gecko.smartmodels.rdf.model.rdf.SubjectNode#getSubjectOf()
	 * @see #getSubjectNode()
	 * @generated
	 */
	EReference getSubjectNode_SubjectOf();

	/**
	 * Returns the meta object for the '{@link org.gecko.smartmodels.rdf.model.rdf.SubjectNode#isTypeOf(java.lang.String) <em>Is Type Of</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Type Of</em>' operation.
	 * @see org.gecko.smartmodels.rdf.model.rdf.SubjectNode#isTypeOf(java.lang.String)
	 * @generated
	 */
	EOperation getSubjectNode__IsTypeOf__String();

	/**
	 * Returns the meta object for the '{@link org.gecko.smartmodels.rdf.model.rdf.SubjectNode#getTypes() <em>Get Types</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Types</em>' operation.
	 * @see org.gecko.smartmodels.rdf.model.rdf.SubjectNode#getTypes()
	 * @generated
	 */
	EOperation getSubjectNode__GetTypes();

	/**
	 * Returns the meta object for the '{@link org.gecko.smartmodels.rdf.model.rdf.SubjectNode#getStringValue(java.lang.String) <em>Get String Value</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get String Value</em>' operation.
	 * @see org.gecko.smartmodels.rdf.model.rdf.SubjectNode#getStringValue(java.lang.String)
	 * @generated
	 */
	EOperation getSubjectNode__GetStringValue__String();

	/**
	 * Returns the meta object for the '{@link org.gecko.smartmodels.rdf.model.rdf.SubjectNode#getValues(java.lang.String) <em>Get Values</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Values</em>' operation.
	 * @see org.gecko.smartmodels.rdf.model.rdf.SubjectNode#getValues(java.lang.String)
	 * @generated
	 */
	EOperation getSubjectNode__GetValues__String();

	/**
	 * Returns the meta object for the '{@link org.gecko.smartmodels.rdf.model.rdf.SubjectNode#getURIValues(java.lang.String) <em>Get URI Values</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get URI Values</em>' operation.
	 * @see org.gecko.smartmodels.rdf.model.rdf.SubjectNode#getURIValues(java.lang.String)
	 * @generated
	 */
	EOperation getSubjectNode__GetURIValues__String();

	/**
	 * Returns the meta object for the '{@link org.gecko.smartmodels.rdf.model.rdf.SubjectNode#getObject(java.lang.String) <em>Get Object</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Object</em>' operation.
	 * @see org.gecko.smartmodels.rdf.model.rdf.SubjectNode#getObject(java.lang.String)
	 * @generated
	 */
	EOperation getSubjectNode__GetObject__String();

	/**
	 * Returns the meta object for class '{@link org.gecko.smartmodels.rdf.model.rdf.NamedGraph <em>Named Graph</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Named Graph</em>'.
	 * @see org.gecko.smartmodels.rdf.model.rdf.NamedGraph
	 * @generated
	 */
	EClass getNamedGraph();

	/**
	 * Returns the meta object for the reference '{@link org.gecko.smartmodels.rdf.model.rdf.NamedGraph#getDocument <em>Document</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Document</em>'.
	 * @see org.gecko.smartmodels.rdf.model.rdf.NamedGraph#getDocument()
	 * @see #getNamedGraph()
	 * @generated
	 */
	EReference getNamedGraph_Document();

	/**
	 * Returns the meta object for class '{@link org.gecko.smartmodels.rdf.model.rdf.BlankNode <em>Blank Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Blank Node</em>'.
	 * @see org.gecko.smartmodels.rdf.model.rdf.BlankNode
	 * @generated
	 */
	EClass getBlankNode();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.smartmodels.rdf.model.rdf.BlankNode#getNodeID <em>Node ID</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Node ID</em>'.
	 * @see org.gecko.smartmodels.rdf.model.rdf.BlankNode#getNodeID()
	 * @see #getBlankNode()
	 * @generated
	 */
	EAttribute getBlankNode_NodeID();

	/**
	 * Returns the meta object for the container reference '{@link org.gecko.smartmodels.rdf.model.rdf.BlankNode#getGraph <em>Graph</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Graph</em>'.
	 * @see org.gecko.smartmodels.rdf.model.rdf.BlankNode#getGraph()
	 * @see #getBlankNode()
	 * @generated
	 */
	EReference getBlankNode_Graph();

	/**
	 * Returns the meta object for class '{@link org.gecko.smartmodels.rdf.model.rdf.TripleNode <em>Triple Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Triple Node</em>'.
	 * @see org.gecko.smartmodels.rdf.model.rdf.TripleNode
	 * @generated
	 */
	EClass getTripleNode();

	/**
	 * Returns the meta object for the reference '{@link org.gecko.smartmodels.rdf.model.rdf.TripleNode#getTriple <em>Triple</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Triple</em>'.
	 * @see org.gecko.smartmodels.rdf.model.rdf.TripleNode#getTriple()
	 * @see #getTripleNode()
	 * @generated
	 */
	EReference getTripleNode_Triple();

	/**
	 * Returns the meta object for class '{@link org.gecko.smartmodels.rdf.model.rdf.Resource <em>Resource</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Resource</em>'.
	 * @see org.gecko.smartmodels.rdf.model.rdf.Resource
	 * @generated
	 */
	EClass getResource();

	/**
	 * Returns the meta object for the container reference '{@link org.gecko.smartmodels.rdf.model.rdf.Resource#getGraph <em>Graph</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Graph</em>'.
	 * @see org.gecko.smartmodels.rdf.model.rdf.Resource#getGraph()
	 * @see #getResource()
	 * @generated
	 */
	EReference getResource_Graph();

	/**
	 * Returns the meta object for the '{@link org.gecko.smartmodels.rdf.model.rdf.Resource#isTypeOf(java.lang.String) <em>Is Type Of</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Type Of</em>' operation.
	 * @see org.gecko.smartmodels.rdf.model.rdf.Resource#isTypeOf(java.lang.String)
	 * @generated
	 */
	EOperation getResource__IsTypeOf__String();

	/**
	 * Returns the meta object for class '{@link org.gecko.smartmodels.rdf.model.rdf.Property <em>Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Property</em>'.
	 * @see org.gecko.smartmodels.rdf.model.rdf.Property
	 * @generated
	 */
	EClass getProperty();

	/**
	 * Returns the meta object for the reference list '{@link org.gecko.smartmodels.rdf.model.rdf.Property#getPredicateOf <em>Predicate Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Predicate Of</em>'.
	 * @see org.gecko.smartmodels.rdf.model.rdf.Property#getPredicateOf()
	 * @see #getProperty()
	 * @generated
	 */
	EReference getProperty_PredicateOf();

	/**
	 * Returns the meta object for class '{@link org.gecko.smartmodels.rdf.model.rdf.Datatype <em>Datatype</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Datatype</em>'.
	 * @see org.gecko.smartmodels.rdf.model.rdf.Datatype
	 * @generated
	 */
	EClass getDatatype();

	/**
	 * Returns the meta object for class '{@link org.gecko.smartmodels.rdf.model.rdf.Literal <em>Literal</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Literal</em>'.
	 * @see org.gecko.smartmodels.rdf.model.rdf.Literal
	 * @generated
	 */
	EClass getLiteral();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.smartmodels.rdf.model.rdf.Literal#getLexicalForm <em>Lexical Form</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Lexical Form</em>'.
	 * @see org.gecko.smartmodels.rdf.model.rdf.Literal#getLexicalForm()
	 * @see #getLiteral()
	 * @generated
	 */
	EAttribute getLiteral_LexicalForm();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.smartmodels.rdf.model.rdf.Literal#getLang <em>Lang</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Lang</em>'.
	 * @see org.gecko.smartmodels.rdf.model.rdf.Literal#getLang()
	 * @see #getLiteral()
	 * @generated
	 */
	EAttribute getLiteral_Lang();

	/**
	 * Returns the meta object for the reference '{@link org.gecko.smartmodels.rdf.model.rdf.Literal#getDatatype <em>Datatype</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Datatype</em>'.
	 * @see org.gecko.smartmodels.rdf.model.rdf.Literal#getDatatype()
	 * @see #getLiteral()
	 * @generated
	 */
	EReference getLiteral_Datatype();

	/**
	 * Returns the meta object for the container reference '{@link org.gecko.smartmodels.rdf.model.rdf.Literal#getGraph <em>Graph</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Graph</em>'.
	 * @see org.gecko.smartmodels.rdf.model.rdf.Literal#getGraph()
	 * @see #getLiteral()
	 * @generated
	 */
	EReference getLiteral_Graph();

	/**
	 * Returns the meta object for class '{@link org.gecko.smartmodels.rdf.model.rdf.RDFSContainer <em>SContainer</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>SContainer</em>'.
	 * @see org.gecko.smartmodels.rdf.model.rdf.RDFSContainer
	 * @generated
	 */
	EClass getRDFSContainer();

	/**
	 * Returns the meta object for class '{@link org.gecko.smartmodels.rdf.model.rdf.RDFBag <em>Bag</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Bag</em>'.
	 * @see org.gecko.smartmodels.rdf.model.rdf.RDFBag
	 * @generated
	 */
	EClass getRDFBag();

	/**
	 * Returns the meta object for the reference list '{@link org.gecko.smartmodels.rdf.model.rdf.RDFBag#getElements <em>Elements</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Elements</em>'.
	 * @see org.gecko.smartmodels.rdf.model.rdf.RDFBag#getElements()
	 * @see #getRDFBag()
	 * @generated
	 */
	EReference getRDFBag_Elements();

	/**
	 * Returns the meta object for class '{@link org.gecko.smartmodels.rdf.model.rdf.RDFAlt <em>Alt</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Alt</em>'.
	 * @see org.gecko.smartmodels.rdf.model.rdf.RDFAlt
	 * @generated
	 */
	EClass getRDFAlt();

	/**
	 * Returns the meta object for the reference list '{@link org.gecko.smartmodels.rdf.model.rdf.RDFAlt#getElements <em>Elements</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Elements</em>'.
	 * @see org.gecko.smartmodels.rdf.model.rdf.RDFAlt#getElements()
	 * @see #getRDFAlt()
	 * @generated
	 */
	EReference getRDFAlt_Elements();

	/**
	 * Returns the meta object for class '{@link org.gecko.smartmodels.rdf.model.rdf.RDFSeq <em>Seq</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Seq</em>'.
	 * @see org.gecko.smartmodels.rdf.model.rdf.RDFSeq
	 * @generated
	 */
	EClass getRDFSeq();

	/**
	 * Returns the meta object for the reference list '{@link org.gecko.smartmodels.rdf.model.rdf.RDFSeq#getElements <em>Elements</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Elements</em>'.
	 * @see org.gecko.smartmodels.rdf.model.rdf.RDFSeq#getElements()
	 * @see #getRDFSeq()
	 * @generated
	 */
	EReference getRDFSeq_Elements();

	/**
	 * Returns the meta object for class '{@link org.gecko.smartmodels.rdf.model.rdf.RDFList <em>List</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>List</em>'.
	 * @see org.gecko.smartmodels.rdf.model.rdf.RDFList
	 * @generated
	 */
	EClass getRDFList();

	/**
	 * Returns the meta object for the reference list '{@link org.gecko.smartmodels.rdf.model.rdf.RDFList#getElements <em>Elements</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Elements</em>'.
	 * @see org.gecko.smartmodels.rdf.model.rdf.RDFList#getElements()
	 * @see #getRDFList()
	 * @generated
	 */
	EReference getRDFList_Elements();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	RDFFactory getRDFFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.gecko.smartmodels.rdf.model.rdf.impl.URIElementImpl <em>URI Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.gecko.smartmodels.rdf.model.rdf.impl.URIElementImpl
		 * @see org.gecko.smartmodels.rdf.model.rdf.impl.RDFPackageImpl#getURIElement()
		 * @generated
		 */
		EClass URI_ELEMENT = eINSTANCE.getURIElement();

		/**
		 * The meta object literal for the '<em><b>URI</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute URI_ELEMENT__URI = eINSTANCE.getURIElement_URI();

		/**
		 * The meta object literal for the '<em><b>Namespace</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference URI_ELEMENT__NAMESPACE = eINSTANCE.getURIElement_Namespace();

		/**
		 * The meta object literal for the '<em><b>Get Local Name</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation URI_ELEMENT___GET_LOCAL_NAME = eINSTANCE.getURIElement__GetLocalName();

		/**
		 * The meta object literal for the '{@link org.gecko.smartmodels.rdf.model.rdf.impl.NamespaceImpl <em>Namespace</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.gecko.smartmodels.rdf.model.rdf.impl.NamespaceImpl
		 * @see org.gecko.smartmodels.rdf.model.rdf.impl.RDFPackageImpl#getNamespace()
		 * @generated
		 */
		EClass NAMESPACE = eINSTANCE.getNamespace();

		/**
		 * The meta object literal for the '<em><b>Prefix</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NAMESPACE__PREFIX = eINSTANCE.getNamespace_Prefix();

		/**
		 * The meta object literal for the '<em><b>Graph</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NAMESPACE__GRAPH = eINSTANCE.getNamespace_Graph();

		/**
		 * The meta object literal for the '{@link org.gecko.smartmodels.rdf.model.rdf.impl.RDFGraphImpl <em>Graph</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.gecko.smartmodels.rdf.model.rdf.impl.RDFGraphImpl
		 * @see org.gecko.smartmodels.rdf.model.rdf.impl.RDFPackageImpl#getRDFGraph()
		 * @generated
		 */
		EClass RDF_GRAPH = eINSTANCE.getRDFGraph();

		/**
		 * The meta object literal for the '<em><b>Nodes</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RDF_GRAPH__NODES = eINSTANCE.getRDFGraph_Nodes();

		/**
		 * The meta object literal for the '<em><b>Resources</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RDF_GRAPH__RESOURCES = eINSTANCE.getRDFGraph_Resources();

		/**
		 * The meta object literal for the '<em><b>Properties</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RDF_GRAPH__PROPERTIES = eINSTANCE.getRDFGraph_Properties();

		/**
		 * The meta object literal for the '<em><b>Blank Nodes</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RDF_GRAPH__BLANK_NODES = eINSTANCE.getRDFGraph_BlankNodes();

		/**
		 * The meta object literal for the '<em><b>Datatypes</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RDF_GRAPH__DATATYPES = eINSTANCE.getRDFGraph_Datatypes();

		/**
		 * The meta object literal for the '<em><b>Literals</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RDF_GRAPH__LITERALS = eINSTANCE.getRDFGraph_Literals();

		/**
		 * The meta object literal for the '<em><b>Triples</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RDF_GRAPH__TRIPLES = eINSTANCE.getRDFGraph_Triples();

		/**
		 * The meta object literal for the '<em><b>Is Equivalent To</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation RDF_GRAPH___IS_EQUIVALENT_TO__RDFGRAPH = eINSTANCE.getRDFGraph__IsEquivalentTo__RDFGraph();

		/**
		 * The meta object literal for the '<em><b>Add</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation RDF_GRAPH___ADD__RDFGRAPH = eINSTANCE.getRDFGraph__Add__RDFGraph();

		/**
		 * The meta object literal for the '<em><b>Union</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation RDF_GRAPH___UNION__RDFGRAPH = eINSTANCE.getRDFGraph__Union__RDFGraph();

		/**
		 * The meta object literal for the '<em><b>Diff</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation RDF_GRAPH___DIFF__RDFGRAPH = eINSTANCE.getRDFGraph__Diff__RDFGraph();

		/**
		 * The meta object literal for the '<em><b>Add Triple</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation RDF_GRAPH___ADD_TRIPLE__NODE_PROPERTY_NODE = eINSTANCE.getRDFGraph__AddTriple__Node_Property_Node();

		/**
		 * The meta object literal for the '<em><b>List Subjects With Property</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation RDF_GRAPH___LIST_SUBJECTS_WITH_PROPERTY__PROPERTY = eINSTANCE.getRDFGraph__ListSubjectsWithProperty__Property();

		/**
		 * The meta object literal for the '<em><b>List Subjects With Property Value</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation RDF_GRAPH___LIST_SUBJECTS_WITH_PROPERTY_VALUE__PROPERTY_NODE = eINSTANCE.getRDFGraph__ListSubjectsWithPropertyValue__Property_Node();

		/**
		 * The meta object literal for the '<em><b>List Subjects</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation RDF_GRAPH___LIST_SUBJECTS = eINSTANCE.getRDFGraph__ListSubjects();

		/**
		 * The meta object literal for the '<em><b>Get Resource</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation RDF_GRAPH___GET_RESOURCE__STRING = eINSTANCE.getRDFGraph__GetResource__String();

		/**
		 * The meta object literal for the '<em><b>Get Property</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation RDF_GRAPH___GET_PROPERTY__STRING = eINSTANCE.getRDFGraph__GetProperty__String();

		/**
		 * The meta object literal for the '<em><b>Get Datatype</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation RDF_GRAPH___GET_DATATYPE__STRING = eINSTANCE.getRDFGraph__GetDatatype__String();

		/**
		 * The meta object literal for the '<em><b>Get Blank Node</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation RDF_GRAPH___GET_BLANK_NODE__STRING = eINSTANCE.getRDFGraph__GetBlankNode__String();

		/**
		 * The meta object literal for the '<em><b>List All Triples</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation RDF_GRAPH___LIST_ALL_TRIPLES = eINSTANCE.getRDFGraph__ListAllTriples();

		/**
		 * The meta object literal for the '<em><b>List All Resources</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation RDF_GRAPH___LIST_ALL_RESOURCES = eINSTANCE.getRDFGraph__ListAllResources();

		/**
		 * The meta object literal for the '<em><b>List All Properties</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation RDF_GRAPH___LIST_ALL_PROPERTIES = eINSTANCE.getRDFGraph__ListAllProperties();

		/**
		 * The meta object literal for the '{@link org.gecko.smartmodels.rdf.model.rdf.impl.DocumentGraphImpl <em>Document Graph</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.gecko.smartmodels.rdf.model.rdf.impl.DocumentGraphImpl
		 * @see org.gecko.smartmodels.rdf.model.rdf.impl.RDFPackageImpl#getDocumentGraph()
		 * @generated
		 */
		EClass DOCUMENT_GRAPH = eINSTANCE.getDocumentGraph();

		/**
		 * The meta object literal for the '<em><b>Namespaces</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DOCUMENT_GRAPH__NAMESPACES = eINSTANCE.getDocumentGraph_Namespaces();

		/**
		 * The meta object literal for the '<em><b>Sub Graphs</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DOCUMENT_GRAPH__SUB_GRAPHS = eINSTANCE.getDocumentGraph_SubGraphs();

		/**
		 * The meta object literal for the '<em><b>Get Named Graph</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation DOCUMENT_GRAPH___GET_NAMED_GRAPH__STRING = eINSTANCE.getDocumentGraph__GetNamedGraph__String();

		/**
		 * The meta object literal for the '{@link org.gecko.smartmodels.rdf.model.rdf.impl.TripleImpl <em>Triple</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.gecko.smartmodels.rdf.model.rdf.impl.TripleImpl
		 * @see org.gecko.smartmodels.rdf.model.rdf.impl.RDFPackageImpl#getTriple()
		 * @generated
		 */
		EClass TRIPLE = eINSTANCE.getTriple();

		/**
		 * The meta object literal for the '<em><b>Subject</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TRIPLE__SUBJECT = eINSTANCE.getTriple_Subject();

		/**
		 * The meta object literal for the '<em><b>Predicate</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TRIPLE__PREDICATE = eINSTANCE.getTriple_Predicate();

		/**
		 * The meta object literal for the '<em><b>Object</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TRIPLE__OBJECT = eINSTANCE.getTriple_Object();

		/**
		 * The meta object literal for the '<em><b>Graph</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TRIPLE__GRAPH = eINSTANCE.getTriple_Graph();

		/**
		 * The meta object literal for the '{@link org.gecko.smartmodels.rdf.model.rdf.impl.NodeImpl <em>Node</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.gecko.smartmodels.rdf.model.rdf.impl.NodeImpl
		 * @see org.gecko.smartmodels.rdf.model.rdf.impl.RDFPackageImpl#getNode()
		 * @generated
		 */
		EClass NODE = eINSTANCE.getNode();

		/**
		 * The meta object literal for the '<em><b>Label</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NODE__LABEL = eINSTANCE.getNode_Label();

		/**
		 * The meta object literal for the '<em><b>Comment</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NODE__COMMENT = eINSTANCE.getNode_Comment();

		/**
		 * The meta object literal for the '<em><b>Object Of</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NODE__OBJECT_OF = eINSTANCE.getNode_ObjectOf();

		/**
		 * The meta object literal for the '{@link org.gecko.smartmodels.rdf.model.rdf.impl.SubjectNodeImpl <em>Subject Node</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.gecko.smartmodels.rdf.model.rdf.impl.SubjectNodeImpl
		 * @see org.gecko.smartmodels.rdf.model.rdf.impl.RDFPackageImpl#getSubjectNode()
		 * @generated
		 */
		EClass SUBJECT_NODE = eINSTANCE.getSubjectNode();

		/**
		 * The meta object literal for the '<em><b>Subject Of</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SUBJECT_NODE__SUBJECT_OF = eINSTANCE.getSubjectNode_SubjectOf();

		/**
		 * The meta object literal for the '<em><b>Is Type Of</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation SUBJECT_NODE___IS_TYPE_OF__STRING = eINSTANCE.getSubjectNode__IsTypeOf__String();

		/**
		 * The meta object literal for the '<em><b>Get Types</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation SUBJECT_NODE___GET_TYPES = eINSTANCE.getSubjectNode__GetTypes();

		/**
		 * The meta object literal for the '<em><b>Get String Value</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation SUBJECT_NODE___GET_STRING_VALUE__STRING = eINSTANCE.getSubjectNode__GetStringValue__String();

		/**
		 * The meta object literal for the '<em><b>Get Values</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation SUBJECT_NODE___GET_VALUES__STRING = eINSTANCE.getSubjectNode__GetValues__String();

		/**
		 * The meta object literal for the '<em><b>Get URI Values</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation SUBJECT_NODE___GET_URI_VALUES__STRING = eINSTANCE.getSubjectNode__GetURIValues__String();

		/**
		 * The meta object literal for the '<em><b>Get Object</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation SUBJECT_NODE___GET_OBJECT__STRING = eINSTANCE.getSubjectNode__GetObject__String();

		/**
		 * The meta object literal for the '{@link org.gecko.smartmodels.rdf.model.rdf.impl.NamedGraphImpl <em>Named Graph</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.gecko.smartmodels.rdf.model.rdf.impl.NamedGraphImpl
		 * @see org.gecko.smartmodels.rdf.model.rdf.impl.RDFPackageImpl#getNamedGraph()
		 * @generated
		 */
		EClass NAMED_GRAPH = eINSTANCE.getNamedGraph();

		/**
		 * The meta object literal for the '<em><b>Document</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NAMED_GRAPH__DOCUMENT = eINSTANCE.getNamedGraph_Document();

		/**
		 * The meta object literal for the '{@link org.gecko.smartmodels.rdf.model.rdf.impl.BlankNodeImpl <em>Blank Node</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.gecko.smartmodels.rdf.model.rdf.impl.BlankNodeImpl
		 * @see org.gecko.smartmodels.rdf.model.rdf.impl.RDFPackageImpl#getBlankNode()
		 * @generated
		 */
		EClass BLANK_NODE = eINSTANCE.getBlankNode();

		/**
		 * The meta object literal for the '<em><b>Node ID</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BLANK_NODE__NODE_ID = eINSTANCE.getBlankNode_NodeID();

		/**
		 * The meta object literal for the '<em><b>Graph</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BLANK_NODE__GRAPH = eINSTANCE.getBlankNode_Graph();

		/**
		 * The meta object literal for the '{@link org.gecko.smartmodels.rdf.model.rdf.impl.TripleNodeImpl <em>Triple Node</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.gecko.smartmodels.rdf.model.rdf.impl.TripleNodeImpl
		 * @see org.gecko.smartmodels.rdf.model.rdf.impl.RDFPackageImpl#getTripleNode()
		 * @generated
		 */
		EClass TRIPLE_NODE = eINSTANCE.getTripleNode();

		/**
		 * The meta object literal for the '<em><b>Triple</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TRIPLE_NODE__TRIPLE = eINSTANCE.getTripleNode_Triple();

		/**
		 * The meta object literal for the '{@link org.gecko.smartmodels.rdf.model.rdf.impl.ResourceImpl <em>Resource</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.gecko.smartmodels.rdf.model.rdf.impl.ResourceImpl
		 * @see org.gecko.smartmodels.rdf.model.rdf.impl.RDFPackageImpl#getResource()
		 * @generated
		 */
		EClass RESOURCE = eINSTANCE.getResource();

		/**
		 * The meta object literal for the '<em><b>Graph</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RESOURCE__GRAPH = eINSTANCE.getResource_Graph();

		/**
		 * The meta object literal for the '<em><b>Is Type Of</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation RESOURCE___IS_TYPE_OF__STRING = eINSTANCE.getResource__IsTypeOf__String();

		/**
		 * The meta object literal for the '{@link org.gecko.smartmodels.rdf.model.rdf.impl.PropertyImpl <em>Property</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.gecko.smartmodels.rdf.model.rdf.impl.PropertyImpl
		 * @see org.gecko.smartmodels.rdf.model.rdf.impl.RDFPackageImpl#getProperty()
		 * @generated
		 */
		EClass PROPERTY = eINSTANCE.getProperty();

		/**
		 * The meta object literal for the '<em><b>Predicate Of</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROPERTY__PREDICATE_OF = eINSTANCE.getProperty_PredicateOf();

		/**
		 * The meta object literal for the '{@link org.gecko.smartmodels.rdf.model.rdf.impl.DatatypeImpl <em>Datatype</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.gecko.smartmodels.rdf.model.rdf.impl.DatatypeImpl
		 * @see org.gecko.smartmodels.rdf.model.rdf.impl.RDFPackageImpl#getDatatype()
		 * @generated
		 */
		EClass DATATYPE = eINSTANCE.getDatatype();

		/**
		 * The meta object literal for the '{@link org.gecko.smartmodels.rdf.model.rdf.impl.LiteralImpl <em>Literal</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.gecko.smartmodels.rdf.model.rdf.impl.LiteralImpl
		 * @see org.gecko.smartmodels.rdf.model.rdf.impl.RDFPackageImpl#getLiteral()
		 * @generated
		 */
		EClass LITERAL = eINSTANCE.getLiteral();

		/**
		 * The meta object literal for the '<em><b>Lexical Form</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LITERAL__LEXICAL_FORM = eINSTANCE.getLiteral_LexicalForm();

		/**
		 * The meta object literal for the '<em><b>Lang</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LITERAL__LANG = eINSTANCE.getLiteral_Lang();

		/**
		 * The meta object literal for the '<em><b>Datatype</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LITERAL__DATATYPE = eINSTANCE.getLiteral_Datatype();

		/**
		 * The meta object literal for the '<em><b>Graph</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LITERAL__GRAPH = eINSTANCE.getLiteral_Graph();

		/**
		 * The meta object literal for the '{@link org.gecko.smartmodels.rdf.model.rdf.impl.RDFSContainerImpl <em>SContainer</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.gecko.smartmodels.rdf.model.rdf.impl.RDFSContainerImpl
		 * @see org.gecko.smartmodels.rdf.model.rdf.impl.RDFPackageImpl#getRDFSContainer()
		 * @generated
		 */
		EClass RDF_SCONTAINER = eINSTANCE.getRDFSContainer();

		/**
		 * The meta object literal for the '{@link org.gecko.smartmodels.rdf.model.rdf.impl.RDFBagImpl <em>Bag</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.gecko.smartmodels.rdf.model.rdf.impl.RDFBagImpl
		 * @see org.gecko.smartmodels.rdf.model.rdf.impl.RDFPackageImpl#getRDFBag()
		 * @generated
		 */
		EClass RDF_BAG = eINSTANCE.getRDFBag();

		/**
		 * The meta object literal for the '<em><b>Elements</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RDF_BAG__ELEMENTS = eINSTANCE.getRDFBag_Elements();

		/**
		 * The meta object literal for the '{@link org.gecko.smartmodels.rdf.model.rdf.impl.RDFAltImpl <em>Alt</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.gecko.smartmodels.rdf.model.rdf.impl.RDFAltImpl
		 * @see org.gecko.smartmodels.rdf.model.rdf.impl.RDFPackageImpl#getRDFAlt()
		 * @generated
		 */
		EClass RDF_ALT = eINSTANCE.getRDFAlt();

		/**
		 * The meta object literal for the '<em><b>Elements</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RDF_ALT__ELEMENTS = eINSTANCE.getRDFAlt_Elements();

		/**
		 * The meta object literal for the '{@link org.gecko.smartmodels.rdf.model.rdf.impl.RDFSeqImpl <em>Seq</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.gecko.smartmodels.rdf.model.rdf.impl.RDFSeqImpl
		 * @see org.gecko.smartmodels.rdf.model.rdf.impl.RDFPackageImpl#getRDFSeq()
		 * @generated
		 */
		EClass RDF_SEQ = eINSTANCE.getRDFSeq();

		/**
		 * The meta object literal for the '<em><b>Elements</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RDF_SEQ__ELEMENTS = eINSTANCE.getRDFSeq_Elements();

		/**
		 * The meta object literal for the '{@link org.gecko.smartmodels.rdf.model.rdf.impl.RDFListImpl <em>List</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.gecko.smartmodels.rdf.model.rdf.impl.RDFListImpl
		 * @see org.gecko.smartmodels.rdf.model.rdf.impl.RDFPackageImpl#getRDFList()
		 * @generated
		 */
		EClass RDF_LIST = eINSTANCE.getRDFList();

		/**
		 * The meta object literal for the '<em><b>Elements</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RDF_LIST__ELEMENTS = eINSTANCE.getRDFList_Elements();

	}

} //RDFPackage
