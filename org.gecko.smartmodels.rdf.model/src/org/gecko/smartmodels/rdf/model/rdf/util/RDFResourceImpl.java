/**
 */
package org.gecko.smartmodels.rdf.model.rdf.util;

import org.eclipse.emf.common.util.URI;

import org.eclipse.emf.ecore.xmi.impl.XMIResourceImpl;

/**
 * <!-- begin-user-doc -->
 * The <b>Resource </b> associated with the package.
 * <!-- end-user-doc -->
 * @see org.gecko.smartmodels.rdf.model.rdf.util.RDFResourceFactoryImpl
 * @generated
 */
public class RDFResourceImpl extends XMIResourceImpl {
	/**
	 * Creates an instance of the resource.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param uri the URI of the new resource.
	 * @generated
	 */
	public RDFResourceImpl(URI uri) {
		super(uri);
	}

} //RDFResourceImpl
