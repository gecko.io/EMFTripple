/**
 * Copyright (c) 2012 - 2022 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v2.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.emftriple.apis;

/**
 * 
 * @author ilenia
 * @since Aug 23, 2022
 */
public final class RDF {

	public static final String NS = "http://www.w3.org/1999/02/22-rdf-syntax-ns#";
	
	public static final String Property = NS + "Property";
	
	public static final String XMLLiteral = NS + "XMLLiteral";
	
	public static final String type = NS + "type";
	
	public static final String Bag = NS + "Bag";
	
	public static final String Alt = NS + "Alt";
	
	public static final String list = NS + "list";
	
	public static final String first = NS + "first";
	
	public static final String rest = NS + "rest";
	
	public static final String nil = NS + "nil";
	
	public static final String Statement = NS + "Statement";
	
	public static final String subject = NS + "subject";
	
	public static final String predicate = NS + "predicate";
	
	public static final String object = NS + "object";
	
	public static final String value = NS + "value";
	
}
