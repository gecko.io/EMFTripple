/**
 * Copyright (c) 2012 - 2018 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.emf.ttl.tests;

import static org.assertj.core.api.Assertions.assertThat;

import org.eclipse.emf.ecore.resource.Resource;
import org.gecko.emf.osgi.ResourceFactoryConfigurator;
import org.junit.jupiter.api.Test;
import org.osgi.test.common.annotation.InjectService;
import org.osgi.test.common.service.ServiceAware;


/**
 * <p>
 * This is a Demo Resource for a Jaxrs Whiteboard 
 * </p>
 * 
 * @since 1.0
 */
public class ExampleIntegrationTest {
	
	@Test 
	public void testConfigure(@InjectService(timeout = 2000, filter="(component.name=EMFTurtleConfigurator)") ServiceAware<ResourceFactoryConfigurator> resFacConfAware) {

		assertThat(resFacConfAware).isNotNull();
		assertThat(resFacConfAware.getServiceReferences()).hasSize(1);
		ResourceFactoryConfigurator resFacConf = resFacConfAware.getService();
		Resource.Factory.Registry registry = Resource.Factory.Registry.INSTANCE;
		resFacConf.configureResourceFactory(registry);
		
		assertThat(registry.getContentTypeToFactoryMap().containsKey("application/tlt"));
		assertThat(registry.getExtensionToFactoryMap().containsKey("ttl"));
	}
}
